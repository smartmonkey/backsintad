package com.practica.core.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.practica.core.sintad.dao.impl.TipoDocumentoDaoImpl;
import com.practica.core.sintad.dao.local.TipoDocumentoDaoLocal;
import com.practica.core.sintad.model.dto.TipoDocumentoDTO;
import com.practica.core.sintad.model.jpa.TipoDocumento;
import com.practica.core.sintad.service.PracticaServiceImpl;
import com.practica.core.util.factory.AccionType;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
///@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(MockitoExtension.class)
public class TipoDocumentoRepositoryTests {

	@InjectMocks
	 private PracticaServiceImpl practicaServiceImpl ;
	 
	 private TipoDocumentoDTO tipoDocumento;
	
	  @BeforeEach
	   void setup(){
		  
		  tipoDocumento = new  TipoDocumentoDTO();
		  tipoDocumento.setCodigo("20");
		  tipoDocumento.setNombre("DNI demo");
		  tipoDocumento.setDescripcion("Tipo Doc DNI");
		  tipoDocumento.setEstado(1L);
		 
	   }
	  
	  
		@DisplayName("Test para guardar un TipoDocumento")
		@Test
		void testGuardarTipoDocumento() {
			// given - dado o condición previa o configuración
			
			TipoDocumentoDTO tipoDoc = new  TipoDocumentoDTO();
			tipoDoc.setIdTipoDocumento(21L);;
			tipoDoc.setCodigo("22");
			tipoDoc.setNombre("Carnet Prueba");
			tipoDoc.setDescripcion("Tipo Doc Carnet");
			tipoDoc.setEstado(1L);
			
			

			// when - acción o el comportamiento que vamos a probar
			TipoDocumentoDTO tipoDocGuardado = practicaServiceImpl.controladorAccionTipoDocumento(tipoDoc, AccionType.CREAR);

			// then - verificar la salida
			assertThat(tipoDocGuardado).isNotNull();
			assertThat(tipoDocGuardado.getIdTipoDocumento()).isGreaterThan(0);
		}
	 
}

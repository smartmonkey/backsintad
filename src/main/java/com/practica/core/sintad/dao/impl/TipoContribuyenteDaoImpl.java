package com.practica.core.sintad.dao.impl;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

import com.practica.core.sintad.dao.local.TipoContribuyenteDaoLocal;
import com.practica.core.sintad.model.dto.TipoContribuyenteDTO;
import com.practica.core.sintad.model.jpa.TipoContribuyente;
import com.practica.core.sintad.model.jpa.TipoDocumento;
import com.practica.core.util.factory.StringUtils;
import com.practica.core.util.generic.dao.impl.GenericDAOImpl;

import jakarta.persistence.Query;

@Repository
public class TipoContribuyenteDaoImpl extends  GenericDAOImpl<Long, TipoContribuyente> implements TipoContribuyenteDaoLocal {

	@Override	 
    public List<TipoContribuyente> listarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente) {
        Query query = generarQueryListaTipoContribuyente(tipoContribuyente, false);
        query.setFirstResult(tipoContribuyente.getStartRow());
        query.setMaxResults(tipoContribuyente.getOffset());
        return query.getResultList();
    }  
	
	

	/**
	 * @param tipoContribuyente
	 * @param esContador
	 * @return
	 */
	private Query generarQueryListaTipoContribuyente(TipoContribuyenteDTO tipoContribuyente, boolean esContador) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" select count(o.idTipoContribuyente) from TipoContribuyente o where 1=1 ");
		} else {
			jpaql.append(" select o from TipoContribuyente o where 1=1 ");           
		}
		if (!StringUtils.isNullOrEmpty(tipoContribuyente.getSearch())) {
			jpaql.append(" and upper(o.nombre) like :search  ");
			parametros.put("search", "%" + tipoContribuyente.getSearch().toUpperCase() + "%");
		} 
		if (!esContador) {
			jpaql.append(" ORDER BY 1 ");
		}
		return createQuery(jpaql.toString(), parametros);
	}
	
	@Override
    public int contarListarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente) {
        int resultado = 0;
        try {
            Query query = generarQueryListaTipoContribuyente(tipoContribuyente, true);
            resultado = ((Long) query.getSingleResult()).intValue();
        } catch (Exception e) {
            resultado = 0;
        }
        return resultado;
    }
	
	 @Override
	    public Long generarIdITipoContribuyente() {
	        Long resultado = 1L;
	        Query query = createQuery("select max(o.idTipoContribuyente) from TipoContribuyente o", null);
	        List<Object> listLong =  query.getResultList();
	        if (listLong != null && listLong.size() > 0 && listLong.get(0) != null)  {
	            Long ultimoIdGenerado = Long.valueOf(listLong.get(0).toString());
	            if (!StringUtils.isNullOrEmpty(ultimoIdGenerado)) {
	                resultado = resultado + ultimoIdGenerado;
	            }
	        }
	        return resultado;
	    }

		@Override
		public List<TipoContribuyente> listarTipoContribuyenteSelect()  {
			List<TipoContribuyente> resultado = null;		
			Map<String, Object> parametros = new HashMap<>();				
			StringBuilder jpaql = new StringBuilder();
			jpaql.append(" from TipoContribuyente o where 1=1");			
			Query query = createQuery(jpaql.toString(), parametros);
			resultado = query.getResultList();	
			return resultado;
		}
}

/**
 * 
 */
package com.practica.core.sintad.dao.local;

import java.util.List;

import com.practica.core.sintad.model.dto.EntidadDTO;
import com.practica.core.sintad.model.jpa.Entidad;
import com.practica.core.util.generic.dao.local.GenericDAOLocal;

/**
 * @author desarrollo
 *
 */
public interface EntidadDaoLocal  extends GenericDAOLocal<Long,Entidad> {
	
	List<Entidad> listarEntidad(EntidadDTO entidad);
	int contarListarEntidad(EntidadDTO entidad);
	 Long generarIdIEntidad();
}
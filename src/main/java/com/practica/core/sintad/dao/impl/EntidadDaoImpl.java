package com.practica.core.sintad.dao.impl;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

import com.practica.core.sintad.dao.local.EntidadDaoLocal;
import com.practica.core.sintad.model.dto.EntidadDTO;
import com.practica.core.sintad.model.jpa.Entidad;
import com.practica.core.sintad.model.jpa.Usuario;
import com.practica.core.util.factory.StringUtils;
import com.practica.core.util.generic.dao.impl.GenericDAOImpl;

import jakarta.persistence.Query;

@Repository
public class EntidadDaoImpl extends  GenericDAOImpl<Long, Entidad> implements EntidadDaoLocal {

	@Override	 
    public List<Entidad> listarEntidad(EntidadDTO entidad) {
        Query query = generarQueryListaEntidad(entidad, false);
        query.setFirstResult(entidad.getStartRow());
        query.setMaxResults(entidad.getOffset());
        return query.getResultList();
    }  
	
	

	/**
	 * @param entidad
	 * @param esContador
	 * @return
	 */
	private Query generarQueryListaEntidad(EntidadDTO entidad, boolean esContador) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" select count(o.idEntidad) from Entidad o where 1=1 ");
		} else {
			jpaql.append(" select o from Entidad o where 1=1 ");           
		}
		if (!StringUtils.isNullOrEmpty(entidad.getSearch())) {
			jpaql.append(" and upper(o.razonSocial) like :search  or upper(o.nroDocumento) like :search  ");
			parametros.put("search", "%" + entidad.getSearch().toUpperCase() + "%");
		} 
		if (!esContador) {
			jpaql.append(" ORDER BY 1 ");
		}
		return createQuery(jpaql.toString(), parametros);
	}
	
	@Override
    public int contarListarEntidad(EntidadDTO entidad) {
        int resultado = 0;
        try {
            Query query = generarQueryListaEntidad(entidad, true);
            resultado = ((Long) query.getSingleResult()).intValue();
        } catch (Exception e) {
            resultado = 0;
        }
        return resultado;
    }
	
	 @Override
	    public Long generarIdIEntidad() {
	        Long resultado = 1L;
	        Query query = createQuery("select max(o.idEntidad) from Entidad o", null);
	        List<Object> listLong =  query.getResultList();
	        if (listLong != null && listLong.size() > 0 && listLong.get(0) != null)  {
	            Long ultimoIdGenerado = Long.valueOf(listLong.get(0).toString());
	            if (!StringUtils.isNullOrEmpty(ultimoIdGenerado)) {
	                resultado = resultado + ultimoIdGenerado;
	            }
	        }
	        return resultado;
	    }

}

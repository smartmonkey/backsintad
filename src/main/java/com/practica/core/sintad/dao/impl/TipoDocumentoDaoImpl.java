package com.practica.core.sintad.dao.impl;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

import com.practica.core.sintad.dao.local.TipoDocumentoDaoLocal;
import com.practica.core.sintad.model.dto.TipoDocumentoDTO;
import com.practica.core.sintad.model.jpa.TipoDocumento;
import com.practica.core.sintad.model.jpa.Usuario;
import com.practica.core.util.factory.StringUtils;
import com.practica.core.util.generic.dao.impl.GenericDAOImpl;

import jakarta.persistence.Query;

@Repository
public class TipoDocumentoDaoImpl extends  GenericDAOImpl<Long, TipoDocumento> implements TipoDocumentoDaoLocal {

	@Override	 
    public List<TipoDocumento> listarTipoDocumento(TipoDocumentoDTO tipoDocumento) {
        Query query = generarQueryListaTipoDocumento(tipoDocumento, false);
        query.setFirstResult(tipoDocumento.getStartRow());
        query.setMaxResults(tipoDocumento.getOffset());
        return query.getResultList();
    }  
	
	

	/**
	 * @param tipoDocumento
	 * @param esContador
	 * @return
	 */
	private Query generarQueryListaTipoDocumento(TipoDocumentoDTO tipoDocumento, boolean esContador) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" select count(o.idTipoDocumento) from TipoDocumento o where 1=1 ");
		} else {
			jpaql.append(" select o from TipoDocumento o where 1=1 ");           
		}
		if (!StringUtils.isNullOrEmpty(tipoDocumento.getSearch())) {
			jpaql.append(" and upper(o.nombre) like :search  or upper(o.codigo) like :search  ");
			parametros.put("search", "%" + tipoDocumento.getSearch().toUpperCase() + "%");
		} 
		if (!esContador) {
			jpaql.append(" ORDER BY o.codigo asc ");
		}
		return createQuery(jpaql.toString(), parametros);
	}
	
	@Override
    public int contarListarTipoDocumento(TipoDocumentoDTO tipoDocumento) {
        int resultado = 0;
        try {
            Query query = generarQueryListaTipoDocumento(tipoDocumento, true);
            resultado = ((Long) query.getSingleResult()).intValue();
        } catch (Exception e) {
            resultado = 0;
        }
        return resultado;
    }
	
	 @Override
	    public Long generarIdITipoDocumento() {
	        Long resultado = 1L;
	        Query query = createQuery("select max(o.idTipoDocumento) from TipoDocumento o", null);
	        List<Object> listLong =  query.getResultList();
	        if (listLong != null && listLong.size() > 0 && listLong.get(0) != null)  {
	            Long ultimoIdGenerado = Long.valueOf(listLong.get(0).toString());
	            if (!StringUtils.isNullOrEmpty(ultimoIdGenerado)) {
	                resultado = resultado + ultimoIdGenerado;
	            }
	        }
	        return resultado;
	    }

		@Override
		public List<TipoDocumento> listarTipoDocumentoSelect()  {
			List<TipoDocumento> resultado = null;		
			Map<String, Object> parametros = new HashMap<>();				
			StringBuilder jpaql = new StringBuilder();
			jpaql.append(" from TipoDocumento o where 1=1");			
			Query query = createQuery(jpaql.toString(), parametros);
			resultado = query.getResultList();	
			return resultado;
		}
}

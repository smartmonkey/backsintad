/**
 * 
 */
package com.practica.core.sintad.dao.local;

import java.util.List;

import com.practica.core.sintad.model.dto.TipoDocumentoDTO;
import com.practica.core.sintad.model.jpa.TipoDocumento;
import com.practica.core.util.generic.dao.local.GenericDAOLocal;

/**
 * @author desarrollo
 *
 */

public interface TipoDocumentoDaoLocal  extends GenericDAOLocal<Long,TipoDocumento> {
	
	List<TipoDocumento> listarTipoDocumento(TipoDocumentoDTO tipoDocumento);
	
	int contarListarTipoDocumento(TipoDocumentoDTO tipoDocumento);
	
	Long generarIdITipoDocumento();
	
	List<TipoDocumento> listarTipoDocumentoSelect();
	 
}
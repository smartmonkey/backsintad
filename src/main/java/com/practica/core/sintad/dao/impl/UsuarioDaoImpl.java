package com.practica.core.sintad.dao.impl;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.practica.core.sintad.dao.local.UsuarioDaoLocal;
import com.practica.core.sintad.model.jpa.Usuario;
import com.practica.core.util.generic.dao.impl.GenericDAOImpl;

import jakarta.persistence.Query;

@Repository
public class UsuarioDaoImpl extends  GenericDAOImpl<Long, Usuario> implements UsuarioDaoLocal {
	
	@Override
	public Usuario validarLogin(String email)  {
	Usuario resultado = null;		
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("email", email);
	//	parametros.put("userPassword", userPassword);		
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(" from Usuario p");
		jpaql.append(" where p.email =:email ");
		Query query = createQuery(jpaql.toString(), parametros);
		List<Usuario> listaUsuario = query.getResultList();
		if (listaUsuario != null && !listaUsuario.isEmpty()) {
			resultado = listaUsuario.get(0);
		}	
		return resultado;
	}
}

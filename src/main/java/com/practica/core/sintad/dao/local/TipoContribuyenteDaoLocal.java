/**
 * 
 */
package com.practica.core.sintad.dao.local;

import java.util.List;

import com.practica.core.sintad.model.dto.TipoContribuyenteDTO;
import com.practica.core.sintad.model.jpa.TipoContribuyente;
import com.practica.core.sintad.model.jpa.TipoDocumento;
import com.practica.core.util.generic.dao.local.GenericDAOLocal;

/**
 * @author desarrollo
 *
 */
public interface TipoContribuyenteDaoLocal  extends GenericDAOLocal<Long,TipoContribuyente> {
	
	List<TipoContribuyente> listarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente);
	
	int contarListarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente);
	
	Long generarIdITipoContribuyente();
	 
	List<TipoContribuyente> listarTipoContribuyenteSelect();
}
/**
 * 
 */
package com.practica.core.sintad.dao.local;

import com.practica.core.sintad.model.jpa.Usuario;
import com.practica.core.util.generic.dao.local.GenericDAOLocal;

/**
 * @author desarrollo
 *
 */
public interface UsuarioDaoLocal  extends GenericDAOLocal<Long,Usuario> {
	
	Usuario validarLogin(String email) ;
}
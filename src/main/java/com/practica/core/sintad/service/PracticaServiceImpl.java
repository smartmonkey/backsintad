/**
 * 
 */
package com.practica.core.sintad.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practica.core.sintad.dao.local.EntidadDaoLocal;
import com.practica.core.sintad.dao.local.TipoContribuyenteDaoLocal;
import com.practica.core.sintad.dao.local.TipoDocumentoDaoLocal;
import com.practica.core.sintad.model.dto.EntidadDTO;
import com.practica.core.sintad.model.dto.TipoContribuyenteDTO;
import com.practica.core.sintad.model.dto.TipoDocumentoDTO;
import com.practica.core.sintad.model.jpa.Entidad;
import com.practica.core.sintad.model.jpa.TipoContribuyente;
import com.practica.core.sintad.model.jpa.TipoDocumento;
import com.practica.core.util.factory.AccionType;
import com.practica.core.util.factory.AtributosEntityCacheUtil;
import com.practica.core.util.factory.TransferDataObjectUtil;

import jakarta.transaction.Transactional;

/**
 * @author desarrollo
 *
 */
@Service
@Transactional
public class PracticaServiceImpl implements PracticaServiceLocal {

	@Autowired
	private TipoDocumentoDaoLocal tipoDocumentoDaoImpl;
	
	@Autowired
	private TipoContribuyenteDaoLocal tipoContribuyenteDaoImpl;
	
	@Autowired
	private EntidadDaoLocal entidadDaoImpl;
	
	
	public PracticaServiceImpl() {
		AtributosEntityCacheUtil.getInstance().sincronizarData("com.practica.core.sintad.model.jpa");
	}

	@Override
	public TipoDocumentoDTO controladorAccionTipoDocumento(TipoDocumentoDTO tipoDocumento,
			AccionType accionType) {
		TipoDocumentoDTO resultado = null;
		TipoDocumento resultadoEntity = null;
		switch (accionType) {
		case CREAR:
			tipoDocumento
					.setIdTipoDocumento(tipoDocumentoDaoImpl.generarIdITipoDocumento());			
			resultadoEntity = TransferDataObjectUtil.transferObjetoEntity(tipoDocumento,
					TipoDocumento.class);
			this.tipoDocumentoDaoImpl.save(resultadoEntity);
			resultado = tipoDocumento;
			break;
		case MODIFICAR:			
			resultadoEntity = TransferDataObjectUtil.transferObjetoEntity(tipoDocumento,
					TipoDocumento.class);
			this.tipoDocumentoDaoImpl.update(resultadoEntity);
			resultado = tipoDocumento;
			break;

		case ELIMINAR:
			resultadoEntity = this.tipoDocumentoDaoImpl.find(TipoDocumento.class,
					tipoDocumento.getIdTipoDocumento());
			this.tipoDocumentoDaoImpl.delete(resultadoEntity);
			resultado = tipoDocumento;
			break;
	
		default:
			break;
		}
		return resultado;
	}	
	@Override
	public List<TipoDocumentoDTO> listarTipoDocumento(TipoDocumentoDTO tipoDocumento){
		return   TransferDataObjectUtil.transferObjetoEntityDTOList(this.tipoDocumentoDaoImpl.listarTipoDocumento(tipoDocumento),TipoDocumentoDTO.class);    
	}
	@Override
	public int contarListarTipoDocumento(TipoDocumentoDTO tipoDocumento){
		return  this.tipoDocumentoDaoImpl.contarListarTipoDocumento(tipoDocumento);
	}
	
	@Override
	public EntidadDTO controladorAccionEntidad(EntidadDTO entidad,
			AccionType accionType) {
		EntidadDTO resultado = null;
		Entidad resultadoEntity = null;
		switch (accionType) {
		case CREAR:
			entidad
					.setIdEntidad(entidadDaoImpl.generarIdIEntidad());			
			resultadoEntity = TransferDataObjectUtil.transferObjetoEntity(entidad,
					Entidad.class,"tipoContribuyente@PK@","tipoDocumento@PK@");
			this.entidadDaoImpl.save(resultadoEntity);
			resultado = entidad;
			break;
		case MODIFICAR:			
			resultadoEntity = TransferDataObjectUtil.transferObjetoEntity(entidad,
					Entidad.class,"tipoContribuyente@PK@","tipoDocumento@PK@");
			this.entidadDaoImpl.update(resultadoEntity);
			resultado = entidad;
			break;

		case ELIMINAR:
			resultadoEntity = this.entidadDaoImpl.find(Entidad.class,
					entidad.getIdEntidad());
			this.entidadDaoImpl.delete(resultadoEntity);
			resultado = entidad;
			break;
	
		default:
			break;
		}
		return resultado;
	}	
	@Override
	public List<EntidadDTO> listarEntidad(EntidadDTO entidad){
		return   TransferDataObjectUtil.transferObjetoEntityDTOList(this.entidadDaoImpl.listarEntidad(entidad),EntidadDTO.class,"tipoContribuyente","tipoDocumento");    
	}
	@Override
	public int contarListarEntidad(EntidadDTO entidad){
		return  this.entidadDaoImpl.contarListarEntidad(entidad);
	}
	
	@Override
	public TipoContribuyenteDTO controladorAccionTipoContribuyente(TipoContribuyenteDTO tipoContribuyente,
			AccionType accionType) {
		TipoContribuyenteDTO resultado = null;
		TipoContribuyente resultadoEntity = null;
		switch (accionType) {
		case CREAR:
			tipoContribuyente
					.setIdTipoContribuyente(tipoContribuyenteDaoImpl.generarIdITipoContribuyente());			
			resultadoEntity = TransferDataObjectUtil.transferObjetoEntity(tipoContribuyente,
					TipoContribuyente.class);
			this.tipoContribuyenteDaoImpl.save(resultadoEntity);
			resultado = tipoContribuyente;
			break;
		case MODIFICAR:			
			resultadoEntity = TransferDataObjectUtil.transferObjetoEntity(tipoContribuyente,
					TipoContribuyente.class);
			this.tipoContribuyenteDaoImpl.update(resultadoEntity);
			resultado = tipoContribuyente;
			break;

		case ELIMINAR:
			resultadoEntity = this.tipoContribuyenteDaoImpl.find(TipoContribuyente.class,
					tipoContribuyente.getIdTipoContribuyente());
			this.tipoContribuyenteDaoImpl.delete(resultadoEntity);
			resultado = tipoContribuyente;
			break;
	
		default:
			break;
		}
		return resultado;
	}	
	@Override
	public List<TipoContribuyenteDTO> listarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente){
		return   TransferDataObjectUtil.transferObjetoEntityDTOList(this.tipoContribuyenteDaoImpl.listarTipoContribuyente(tipoContribuyente),TipoContribuyenteDTO.class);    
	}
	@Override
	public int contarListarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente){
		return  this.tipoContribuyenteDaoImpl.contarListarTipoContribuyente(tipoContribuyente);
	}
	
	
	@Override
	public List<TipoDocumentoDTO> listarTipoDocumentoSelect(){
		return   TransferDataObjectUtil.transferObjetoEntityDTOList(this.tipoDocumentoDaoImpl.listarTipoDocumentoSelect(),TipoDocumentoDTO.class);    
	}
	
	@Override
	public List<TipoContribuyenteDTO> listarTipoContribuyenteSelect(){
		return   TransferDataObjectUtil.transferObjetoEntityDTOList(this.tipoContribuyenteDaoImpl.listarTipoContribuyenteSelect(),TipoContribuyenteDTO.class);    
	}
}

/**
 * 
 */
package com.practica.core.sintad.service;

import java.util.List;

import com.practica.core.sintad.model.dto.EntidadDTO;
import com.practica.core.sintad.model.dto.TipoContribuyenteDTO;
import com.practica.core.sintad.model.dto.TipoDocumentoDTO;
import com.practica.core.util.factory.AccionType;

/**
 * @author desarrollo
 *
 */
public interface PracticaServiceLocal {

	TipoDocumentoDTO controladorAccionTipoDocumento(TipoDocumentoDTO tipoDocumento,AccionType accionType); 
	
	List<TipoDocumentoDTO> listarTipoDocumento(TipoDocumentoDTO tipoDocumento);
	
	int contarListarTipoDocumento(TipoDocumentoDTO tipoDocumento);
	
	TipoContribuyenteDTO controladorAccionTipoContribuyente(TipoContribuyenteDTO tipoContribuyente,AccionType accionType); 
	
	List<TipoContribuyenteDTO> listarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente);
	
	int contarListarTipoContribuyente(TipoContribuyenteDTO tipoContribuyente);
	
	EntidadDTO controladorAccionEntidad(EntidadDTO entidad,AccionType accionType); 

	List<EntidadDTO> listarEntidad(EntidadDTO entidad);
	
	int contarListarEntidad(EntidadDTO entidad);
	
	List<TipoDocumentoDTO> listarTipoDocumentoSelect();
	
	List<TipoContribuyenteDTO> listarTipoContribuyenteSelect();
	
}

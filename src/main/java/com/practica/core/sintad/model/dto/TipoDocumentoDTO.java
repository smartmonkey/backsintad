package com.practica.core.sintad.model.dto;

import java.io.Serializable;

import com.practica.core.util.model.BaseGeneric;

public class TipoDocumentoDTO extends BaseGeneric implements Serializable  {

	 private static final long serialVersionUID = 1L;
	 
	 private Long idTipoDocumento;
	 
	 private String codigo;
	 
	 private String nombre;
	 
	 private String descripcion;
	 
	 private Long estado;
	 
	 

	public TipoDocumentoDTO() {
		super();
	}
	
	

	/**
	 * @param idTipoDocumento
	 * @param codigo
	 * @param descripcion
	 * @param estado
	 */
	public TipoDocumentoDTO(Long idTipoDocumento, String codigo,String nombre, String descripcion, Long estado) {
		this.idTipoDocumento = idTipoDocumento;
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.estado = estado;
	}



	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}



	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	/**
	 * @return the idTipoDocumento
	 */
	public Long getIdTipoDocumento() {
		return idTipoDocumento;
	}

	/**
	 * @param idTipoDocumento the idTipoDocumento to set
	 */
	public void setIdTipoDocumento(Long idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the estado
	 */
	public Long getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(Long estado) {
		this.estado = estado;
	}
	 
	 /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TipoDocumentoDTO other = (TipoDocumentoDTO) obj;
        if (idTipoDocumento == null) {
            if (other.idTipoDocumento != null) {
                return false;
            }
        } else if (!idTipoDocumento.equals(other.idTipoDocumento)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TipoDocumentoDTO [idTipoDocumento=" + idTipoDocumento + "]";
    } 
}

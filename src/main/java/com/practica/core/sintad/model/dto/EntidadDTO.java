package com.practica.core.sintad.model.dto;

import java.io.Serializable;

import com.practica.core.sintad.model.jpa.TipoContribuyente;
import com.practica.core.sintad.model.jpa.TipoDocumento;
import com.practica.core.util.model.BaseGeneric;

public class EntidadDTO extends BaseGeneric implements Serializable  {

	 private static final long serialVersionUID = 1L;
	 
	 private Long idEntidad;
	 
	 private TipoDocumentoDTO tipoDocumento;
	 
	 private TipoContribuyenteDTO tipoContribuyente;
	 
	 private String nroDocumento;
	 
	 private String razonSocial;
	 
	 private String nombreComercial;
	 
	private String direccion;
	
	private String telefono;
	 
	 private Long estado;
	 
	 

	public EntidadDTO() {
		super();
	}
	

	 


	/**
	 * @param idEntidad
	 * @param tipoDocumento
	 * @param tipoContribuyente
	 * @param nroDocumento
	 * @param razonSocial
	 * @param nombreComercial
	 * @param direccion
	 * @param telefono
	 * @param estado
	 */
	public EntidadDTO(Long idEntidad, TipoDocumentoDTO tipoDocumento, TipoContribuyenteDTO tipoContribuyente,
			String nroDocumento, String razonSocial, String nombreComercial, String direccion, String telefono,
			Long estado) {
		this.idEntidad = idEntidad;
		this.tipoDocumento = tipoDocumento;
		this.tipoContribuyente = tipoContribuyente;
		this.nroDocumento = nroDocumento;
		this.razonSocial = razonSocial;
		this.nombreComercial = nombreComercial;
		this.direccion = direccion;
		this.telefono = telefono;
		this.estado = estado;
	}





	/**
	 * @return the idEntidad
	 */
	public Long getIdEntidad() {
		return idEntidad;
	}




	/**
	 * @return the tipoContribuyente
	 */
	public TipoContribuyenteDTO getTipoContribuyente() {
		return tipoContribuyente;
	}



	/**
	 * @return the nroDocumento
	 */
	public String getNroDocumento() {
		return nroDocumento;
	}



	/**
	 * @return the razonSocial
	 */
	public String getRazonSocial() {
		return razonSocial;
	}



	/**
	 * @return the nombreComercial
	 */
	public String getNombreComercial() {
		return nombreComercial;
	}



	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}



	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}



	/**
	 * @return the estado
	 */
	public Long getEstado() {
		return estado;
	}



	/**
	 * @param idEntidad the idEntidad to set
	 */
	public void setIdEntidad(Long idEntidad) {
		this.idEntidad = idEntidad;
	}






	/**
	 * @return the tipoDocumento
	 */
	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}





	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(TipoDocumentoDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}





	/**
	 * @param tipoContribuyente the tipoContribuyente to set
	 */
	public void setTipoContribuyente(TipoContribuyenteDTO tipoContribuyente) {
		this.tipoContribuyente = tipoContribuyente;
	}



	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}



	/**
	 * @param razonSocial the razonSocial to set
	 */
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}



	/**
	 * @param nombreComercial the nombreComercial to set
	 */
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}



	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}



	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



	/**
	 * @param estado the estado to set
	 */
	public void setEstado(Long estado) {
		this.estado = estado;
	}



	/* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EntidadDTO other = (EntidadDTO) obj;
        if (idEntidad == null) {
            if (other.idEntidad != null) {
                return false;
            }
        } else if (!idEntidad.equals(other.idEntidad)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "EntidadDTO [idEntidad=" + idEntidad + "]";
    } 
}

/**
 * 
 */
package com.practica.core.sintad.model.jpa;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @author desarrollo
 *
 */
@Entity
@Table(name = "tb_entidad")
@Getter
@Setter
public class Entidad implements Serializable {
	
	 /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
	
	@Id	
	@Column(name = "id_entidad")
	private Long idEntidad;	
	 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id_tipo_documento")
    private TipoDocumento tipoDocumento;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_contribuyente", referencedColumnName = "id_tipo_contribuyente")
    private TipoContribuyente tipoContribuyente;
	
	@Column(name = "nro_documento")
	private String nroDocumento;
	
	@Column(name = "razon_social")
	private String razonSocial;
	
	@Column(name = "nombre_comercial")
	private String nombreComercial;
	
	@Column(name = "direccion")
	private String direccion;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "estado")
	private Long estado;

	

	/**
	 * 
	 */
	public Entidad() {
		super();
	}



	
	 /**
	 * @param idEntidad
	 * @param tipoDocumento
	 * @param tipoContribuyente
	 * @param nroDocumento
	 * @param razonSocial
	 * @param nombreComercial
	 * @param direccion
	 * @param telefono
	 * @param estado
	 */
	public Entidad(Long idEntidad, TipoDocumento tipoDocumento, TipoContribuyente tipoContribuyente,
			String nroDocumento, String razonSocial, String nombreComercial, String direccion, String telefono,
			Long estado) {
		this.idEntidad = idEntidad;
		this.tipoDocumento = tipoDocumento;
		this.tipoContribuyente = tipoContribuyente;
		this.nroDocumento = nroDocumento;
		this.razonSocial = razonSocial;
		this.nombreComercial = nombreComercial;
		this.direccion = direccion;
		this.telefono = telefono;
		this.estado = estado;
	}




	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idEntidad == null) ? 0 : idEntidad.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Entidad other = (Entidad) obj;
        if (idEntidad == null) {
            if (other.idEntidad != null) {
                return false;
            }
        } else if (!idEntidad.equals(other.idEntidad)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Entidad [idEntidad=" + idEntidad + "]";
    }

}

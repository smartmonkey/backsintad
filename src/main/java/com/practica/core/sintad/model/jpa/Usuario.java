/**
 * 
 */
package com.practica.core.sintad.model.jpa;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @author desarrollo
 *
 */
@Entity
@Table(name = "tb_usuario")
@Getter
@Setter
public class Usuario implements Serializable {
	
	 /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
	
	@Id	
	@Column(name = "idusuario")
	private Long idUsuario;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "email")
	private String email;
	
	
	@Column(name = "password")
	private String password;

	

	/**
	 * 
	 */
	public Usuario() {
		super();
	}



	
	 /**
	 * @param idUsuario
	 * @param nombre
	 * @param email
	 * @param password
	 */
	public Usuario(Long idUsuario, String nombre, String email, String password) {
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.email = email;
		this.password = password;
	}


	 /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idUsuario == null) ? 0 : idUsuario.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Usuario other = (Usuario) obj;
        if (idUsuario == null) {
            if (other.idUsuario != null) {
                return false;
            }
        } else if (!idUsuario.equals(other.idUsuario)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Usuario [idUsuario=" + idUsuario + "]";
    }




}

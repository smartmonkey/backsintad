/**
 * 
 */
package com.practica.core.sintad.model.jpa;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @author desarrollo
 *
 */
@Entity
@Table(name = "tb_tipo_contribuyente")
@Getter
@Setter
public class TipoContribuyente implements Serializable {
	
	 /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
	
	@Id	
	@Column(name = "id_tipo_contribuyente")
	private Long idTipoContribuyente;
	
	@Column(name = "nombre")
	private String nombre;	

	@Column(name = "estado")
	private Long estado;

	

	/**
	 * 
	 */
	public TipoContribuyente() {
		super();
	}




	
	 /**
	 * @param idTipoContribuyente
	 * @param nombre
	 * @param estado
	 */
	public TipoContribuyente(Long idTipoContribuyente, String nombre, Long estado) {
		this.idTipoContribuyente = idTipoContribuyente;
		this.nombre = nombre;
		this.estado = estado;
	}





	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idTipoContribuyente == null) ? 0 : idTipoContribuyente.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TipoContribuyente other = (TipoContribuyente) obj;
        if (idTipoContribuyente == null) {
            if (other.idTipoContribuyente != null) {
                return false;
            }
        } else if (!idTipoContribuyente.equals(other.idTipoContribuyente)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TipoContribuyente [idTipoContribuyente=" + idTipoContribuyente + "]";
    }

}

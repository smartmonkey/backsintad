/**
 * 
 */
package com.practica.core.sintad.model.jpa;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author desarrollo
 *
 */
@Entity
@Table(name = "tb_tipo_documento")

public class TipoDocumento implements Serializable {
	
	 /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
	
	@Id	
	@Column(name = "id_tipo_documento")
	private Long idTipoDocumento;
	
	@Column(name = "codigo")
	private String codigo;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "estado")
	private Long estado;

	

	/**
	 * 
	 */
	public TipoDocumento() {
		super();
	}



	/**
	 * @param idTipoDocumento
	 * @param codigo
	 * @param descripcion
	 * @param estado
	 */
	public TipoDocumento(Long idTipoDocumento, String codigo,String nombre, String descripcion, Long estado) {
		super();
		this.idTipoDocumento = idTipoDocumento;
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.estado = estado;
	}
	
	
	
	
	 /**
	 * @return the idTipoDocumento
	 */
	public Long getIdTipoDocumento() {
		return idTipoDocumento;
	}



	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}



	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}



	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}



	/**
	 * @return the estado
	 */
	public Long getEstado() {
		return estado;
	}



	/**
	 * @param idTipoDocumento the idTipoDocumento to set
	 */
	public void setIdTipoDocumento(Long idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}



	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	/**
	 * @param estado the estado to set
	 */
	public void setEstado(Long estado) {
		this.estado = estado;
	}



	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idTipoDocumento == null) ? 0 : idTipoDocumento.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TipoDocumento other = (TipoDocumento) obj;
        if (idTipoDocumento == null) {
            if (other.idTipoDocumento != null) {
                return false;
            }
        } else if (!idTipoDocumento.equals(other.idTipoDocumento)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TipoDocumento [idTipoDocumento=" + idTipoDocumento + "]";
    }

}

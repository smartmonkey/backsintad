package com.practica.core.sintad.model.dto;

import java.io.Serializable;

import com.practica.core.util.model.BaseGeneric;

public class TipoContribuyenteDTO extends BaseGeneric implements Serializable  {

	 private static final long serialVersionUID = 1L;
	 
	 private Long idTipoContribuyente;
	 
	 private String nombre;	
	 
	 private Long estado;
	 
	 

	public TipoContribuyenteDTO() {
		super();
	}
	
	

	 
	 /**
	 * @param idTipoContribuyente
	 * @param nombre
	 * @param estado
	 */
	public TipoContribuyenteDTO(Long idTipoContribuyente, String nombre, Long estado) {
		this.idTipoContribuyente = idTipoContribuyente;
		this.nombre = nombre;
		this.estado = estado;
	}




	/**
	 * @return the idTipoContribuyente
	 */
	public Long getIdTipoContribuyente() {
		return idTipoContribuyente;
	}




	/**
	 * @param idTipoContribuyente the idTipoContribuyente to set
	 */
	public void setIdTipoContribuyente(Long idTipoContribuyente) {
		this.idTipoContribuyente = idTipoContribuyente;
	}




	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}




	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	/**
	 * @return the estado
	 */
	public Long getEstado() {
		return estado;
	}




	/**
	 * @param estado the estado to set
	 */
	public void setEstado(Long estado) {
		this.estado = estado;
	}




	/* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TipoContribuyenteDTO other = (TipoContribuyenteDTO) obj;
        if (idTipoContribuyente == null) {
            if (other.idTipoContribuyente != null) {
                return false;
            }
        } else if (!idTipoContribuyente.equals(other.idTipoContribuyente)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TipoContribuyenteDTO [idTipoContribuyente=" + idTipoContribuyente + "]";
    } 
}

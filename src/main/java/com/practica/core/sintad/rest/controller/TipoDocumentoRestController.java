/**
 * 
 */
package com.practica.core.sintad.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.practica.core.sintad.model.dto.TipoDocumentoDTO;
import com.practica.core.sintad.service.PracticaServiceLocal;
import com.practica.core.util.factory.AccionType;
import com.practica.core.util.factory.TransferDataObjectUtil;
import com.practica.core.util.generic.rest.GenericServiceRestImpl;
import com.practica.core.util.vo.ResultadoRestVO;

/**
 * @author desarrollo
 *
 */
@RestController
@RequestMapping("/sintad/tipoDocumento")
public class TipoDocumentoRestController extends GenericServiceRestImpl  {

	@Autowired
	private transient PracticaServiceLocal practicaServiceLocal;
	
	
	@PostMapping
	public ResultadoRestVO<TipoDocumentoDTO> crear(@RequestBody TipoDocumentoDTO tipoDocumento) throws Exception {
		return controladorAccion(tipoDocumento,AccionType.CREAR);
	}
	@PutMapping
	public ResultadoRestVO<TipoDocumentoDTO> modificar(@RequestBody TipoDocumentoDTO tipoDocumento) throws Exception {
		return controladorAccion(tipoDocumento,AccionType.MODIFICAR);
	}
	@DeleteMapping("/{id}")
	public ResultadoRestVO<TipoDocumentoDTO> eliminar(@PathVariable("id") Long idtipoDocumento) throws Exception {
		TipoDocumentoDTO tipoDocumento = new TipoDocumentoDTO();
		tipoDocumento.setIdTipoDocumento(idtipoDocumento);		
		return controladorAccion(tipoDocumento,AccionType.ELIMINAR);
	}

	private ResultadoRestVO<TipoDocumentoDTO> controladorAccion(TipoDocumentoDTO tipoDocumento, AccionType accionType){
		ResultadoRestVO<TipoDocumentoDTO> resultado = new ResultadoRestVO<>();
		 try {
    		resultado.setObjetoResultado(practicaServiceLocal.controladorAccionTipoDocumento(tipoDocumento,accionType));
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	
	
	@GetMapping("/listar")
	public ResultadoRestVO<TipoDocumentoDTO> listartipoDocumento(WebRequest info){
		ResultadoRestVO<TipoDocumentoDTO> resultado = new ResultadoRestVO<>();
		TipoDocumentoDTO tipoDocumento = transferUriInfo(info);
		 try {
			resultado.setListaResultado(practicaServiceLocal.listarTipoDocumento(tipoDocumento));
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	@GetMapping("/contar")
	public ResultadoRestVO<TipoDocumentoDTO> contartipoDocumento(WebRequest info){
		ResultadoRestVO<TipoDocumentoDTO> resultado = new ResultadoRestVO<>();
		TipoDocumentoDTO tipoDocumento = transferUriInfo(info);
		 try {
			 resultado.setContador(practicaServiceLocal.contarListarTipoDocumento(tipoDocumento));
			 if (resultado.isData()) {
				resultado.setListaResultado(practicaServiceLocal.listarTipoDocumento(tipoDocumento));
			 }
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	private TipoDocumentoDTO transferUriInfo(WebRequest info) {
		return TransferDataObjectUtil.transferObjetoEntityGetRestDTO(info,TipoDocumentoDTO.class);
	}
	
	@GetMapping("/listarTipoDocumentoSelect")
	public ResultadoRestVO<TipoDocumentoDTO> listarTipoDocumentoSelect(WebRequest info){
		ResultadoRestVO<TipoDocumentoDTO> resultado = new ResultadoRestVO<>();
		 try {
			resultado.setListaResultado(practicaServiceLocal.listarTipoDocumentoSelect());
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
}

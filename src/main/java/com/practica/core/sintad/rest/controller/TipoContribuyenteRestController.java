/**
 * 
 */
package com.practica.core.sintad.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.practica.core.sintad.model.dto.TipoContribuyenteDTO;
import com.practica.core.sintad.service.PracticaServiceLocal;
import com.practica.core.util.factory.AccionType;
import com.practica.core.util.factory.TransferDataObjectUtil;
import com.practica.core.util.generic.rest.GenericServiceRestImpl;
import com.practica.core.util.vo.ResultadoRestVO;

/**
 * @author desarrollo
 *
 */
@RestController
@RequestMapping("/sintad/tipoContribuyente")
public class TipoContribuyenteRestController extends GenericServiceRestImpl  {

	@Autowired
	private transient PracticaServiceLocal practicaServiceLocal;
	
	
	@PostMapping
	public ResultadoRestVO<TipoContribuyenteDTO> crear(@RequestBody TipoContribuyenteDTO tipoContribuyente) throws Exception {
		return controladorAccion(tipoContribuyente,AccionType.CREAR);
	}
	@PutMapping
	public ResultadoRestVO<TipoContribuyenteDTO> modificar(@RequestBody TipoContribuyenteDTO tipoContribuyente) throws Exception {
		return controladorAccion(tipoContribuyente,AccionType.MODIFICAR);
	}
	@DeleteMapping("/{id}")
	public ResultadoRestVO<TipoContribuyenteDTO> eliminar(@PathVariable("id") Long idtipoContribuyente) throws Exception {
		TipoContribuyenteDTO tipoContribuyente = new TipoContribuyenteDTO();
		tipoContribuyente.setIdTipoContribuyente(idtipoContribuyente);		
		return controladorAccion(tipoContribuyente,AccionType.ELIMINAR);
	}

	private ResultadoRestVO<TipoContribuyenteDTO> controladorAccion(TipoContribuyenteDTO tipoContribuyente, AccionType accionType){
		ResultadoRestVO<TipoContribuyenteDTO> resultado = new ResultadoRestVO<>();
		 try {
    		resultado.setObjetoResultado(practicaServiceLocal.controladorAccionTipoContribuyente(tipoContribuyente,accionType));
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	
	
	@GetMapping("/listar")
	public ResultadoRestVO<TipoContribuyenteDTO> listartipoContribuyente(WebRequest info){
		ResultadoRestVO<TipoContribuyenteDTO> resultado = new ResultadoRestVO<>();
		TipoContribuyenteDTO tipoContribuyente = transferUriInfo(info);
		 try {
			resultado.setListaResultado(practicaServiceLocal.listarTipoContribuyente(tipoContribuyente));
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	@GetMapping("/contar")
	public ResultadoRestVO<TipoContribuyenteDTO> contartipoContribuyente(WebRequest info){
		ResultadoRestVO<TipoContribuyenteDTO> resultado = new ResultadoRestVO<>();
		TipoContribuyenteDTO tipoContribuyente = transferUriInfo(info);
		 try {
			 resultado.setContador(practicaServiceLocal.contarListarTipoContribuyente(tipoContribuyente));
			 if (resultado.isData()) {
				resultado.setListaResultado(practicaServiceLocal.listarTipoContribuyente(tipoContribuyente));
			 }
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	private TipoContribuyenteDTO transferUriInfo(WebRequest info) {
		return TransferDataObjectUtil.transferObjetoEntityGetRestDTO(info,TipoContribuyenteDTO.class);
	}
	
	@GetMapping("/listarTipoContribuyenteSelect")
	public ResultadoRestVO<TipoContribuyenteDTO> listarTipoContribuyenteSelect(WebRequest info){
		ResultadoRestVO<TipoContribuyenteDTO> resultado = new ResultadoRestVO<>();		
		 try {
			resultado.setListaResultado(practicaServiceLocal.listarTipoContribuyenteSelect());
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
}

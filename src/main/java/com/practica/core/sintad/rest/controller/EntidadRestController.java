/**
 * 
 */
package com.practica.core.sintad.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.practica.core.sintad.model.dto.EntidadDTO;
import com.practica.core.sintad.service.PracticaServiceLocal;
import com.practica.core.util.factory.AccionType;
import com.practica.core.util.factory.TransferDataObjectUtil;
import com.practica.core.util.generic.rest.GenericServiceRestImpl;
import com.practica.core.util.vo.ResultadoRestVO;

/**
 * @author desarrollo
 *
 */
@RestController
@RequestMapping("/sintad/entidad")
public class EntidadRestController extends GenericServiceRestImpl  {

	@Autowired
	private transient PracticaServiceLocal practicaServiceLocal;
	
	
	@PostMapping
	public ResultadoRestVO<EntidadDTO> crear(@RequestBody EntidadDTO entidad) throws Exception {
		return controladorAccion(entidad,AccionType.CREAR);
	}
	@PutMapping
	public ResultadoRestVO<EntidadDTO> modificar(@RequestBody EntidadDTO entidad) throws Exception {
		return controladorAccion(entidad,AccionType.MODIFICAR);
	}
	@DeleteMapping("/{id}")
	public ResultadoRestVO<EntidadDTO> eliminar(@PathVariable("id") Long identidad) throws Exception {
		EntidadDTO entidad = new EntidadDTO();
		entidad.setIdEntidad(identidad);		
		return controladorAccion(entidad,AccionType.ELIMINAR);
	}

	private ResultadoRestVO<EntidadDTO> controladorAccion(EntidadDTO entidad, AccionType accionType){
		ResultadoRestVO<EntidadDTO> resultado = new ResultadoRestVO<>();
		 try {
    		resultado.setObjetoResultado(practicaServiceLocal.controladorAccionEntidad(entidad,accionType));
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	
	
	@GetMapping("/listar")
	public ResultadoRestVO<EntidadDTO> listarentidad(WebRequest info){
		ResultadoRestVO<EntidadDTO> resultado = new ResultadoRestVO<>();
		EntidadDTO entidad = transferUriInfo(info);
		 try {
			resultado.setListaResultado(practicaServiceLocal.listarEntidad(entidad));
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	@GetMapping("/contar")
	public ResultadoRestVO<EntidadDTO> contarentidad(WebRequest info){
		ResultadoRestVO<EntidadDTO> resultado = new ResultadoRestVO<>();
		EntidadDTO entidad = transferUriInfo(info);
		 try {
			 resultado.setContador(practicaServiceLocal.contarListarEntidad(entidad));
			 if (resultado.isData()) {
				resultado.setListaResultado(practicaServiceLocal.listarEntidad(entidad));
			 }
		} catch (Exception e) {
			parsearResultadoError(e, resultado);
		}
		return resultado;
	}
	
	private EntidadDTO transferUriInfo(WebRequest info) {
		return TransferDataObjectUtil.transferObjetoEntityGetRestDTO(info,EntidadDTO.class);
	}
	
}

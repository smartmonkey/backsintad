package com.practica.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DemoApplication  {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
/*	@EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
				.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/user").permitAll()
				.anyRequest().authenticated();
		}
	}*/
/*	@Value("#{'${allowed.origins}'.split(',')}")
	private List<String> rawOrigins;
	@Configuration
	@EnableWebMvc
	public class WebConfig implements WebMvcConfigurer {

	  @Override
	  public void addCorsMappings(CorsRegistry registry) {

	    registry.addMapping("/sintad/**")
	      .allowedOrigins(getOrigin())	      
	      .allowedMethods("GET", "POST","PUT","DELETE","OPTIONS")
	      .allowedHeaders("service_key", "auth_token", "Authorization","Accept","version","Content-Type")
	      .allowCredentials(true);

	    // Add more mappings...
	  }
	}

	public String[] getOrigin() {
	    int size = rawOrigins.size();
	    String[] originArray = new String[size];
	    return rawOrigins.toArray(originArray);
	}*/
}

package com.practica.core.security.rest.controller;

import java.security.GeneralSecurityException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practica.core.sintad.model.dto.UsuarioDTO;
import com.practica.core.util.generic.rest.AppHttpHeaderNames;
import com.practica.core.util.generic.rest.GenericServiceRestImpl;
import com.practica.core.util.security.jwt.AppAuthenticator;
import com.practica.core.util.vo.ResultadoRestVO;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/sintad/login")
public class LoginRestController  extends GenericServiceRestImpl{
		
	@Resource(name = "appAuthenticator")
	AppAuthenticator appAuthenticator;
	
	
	@PostMapping
	public ResultadoRestVO<UsuarioDTO>  login(@RequestHeader HttpHeaders httpHeaders, @RequestBody User user ) {
		ResultadoRestVO<UsuarioDTO> resultado = new ResultadoRestVO<>();
        String serviceKey = httpHeaders.getFirst( AppHttpHeaderNames.SERVICE_KEY );
        try {
            Map<String,Object> resulTempMap = appAuthenticator.login( serviceKey, user.getEmail(), user.getToken() );
            String authToken  = (String) resulTempMap.get("authToken");
            UsuarioDTO usuarioDTO = (UsuarioDTO) resulTempMap.get("usuario");
            resultado.setObjetoResultado(usuarioDTO);
            resultado.setAuthToken(authToken);
        } catch ( Exception ex ) {
        	resultado.setError(true);
            resultado.setCodigoError(HttpStatus.UNAUTHORIZED.value() + "");
            resultado.setMensajeError("El usuario o password es incorrecto ==> " + ex.getMessage());
        }
        return resultado;
	}
	
	@PutMapping
	public ResponseEntity logout(@RequestHeader HttpHeaders httpHeaders,HttpServletRequest request, HttpServletResponse response) {
	        try {
	            String serviceKey = httpHeaders.getFirst( AppHttpHeaderNames.SERVICE_KEY );
	            String authToken = httpHeaders.getFirst( AppHttpHeaderNames.AUTH_TOKEN );
	            appAuthenticator.logout( serviceKey, request, response);
	            return getNoCacheResponseBuilder( HttpStatus.NO_CONTENT );
	        } catch ( final GeneralSecurityException ex ) {
	            return getNoCacheResponseBuilder( HttpStatus.INTERNAL_SERVER_ERROR );
	        }
	    }
	
	private ResponseEntity getNoCacheResponseBuilder( HttpStatus status ) {
	       // CacheControl cc = CacheControl.noCache();
	        /*cc.setNoCache( true );
	        cc.setMaxAge( -1 );
	        cc.setMustRevalidate( true );*/

	        return new ResponseEntity(status );
	    }
}

/**
 * 
 */
package com.practica.core.util.vo;

/**
 * @author desarrollo
 *
 */
public class ConfiguracionUtil {
	
	//Sincronizado con el properties :pwrconfutil.properties
	/** El CANTIDAD_INTENTOS_COLA. */
	public static String CANTIDAD_INTENTOS_COLA = "cantidad.intentos.cola";
	/** El CANTIDAD_PAGINA. */
	public static String CANTIDAD_PAGINA = "cantidad.pagina";
	/** El CANTIDAD_PAGINA_MODAL. */
	public static String CANTIDAD_PAGINA_MODAL = "cantidad.pagina.modal";
	/** El CANTIDAD_PAGINA_INICIO_ACCESO_DIRECTO. */
	public static String CANTIDAD_PAGINA_INICIO_ACCESO_DIRECTO = "cantidad.pagina.inicio.acceso.directo";
	
	
	public static String CARACTER_FORMATEO_NUMERICO= "caracter.formateo.numerico";
	public static String START_ROW = "reporteDetalleResumenProduccion.startrow";
	
	/**
	 * Obtiene PWR conf util int.
	 *
	 * @param key el key
	 * @return PWR conf util int
	 */
	public static int getPWRConfUtilInt(String key) {
		return ConfiguracionCacheUtil.getInstance().getPwrConfUtilInt(key);
	}
	public static String getPwrConfUtil(String key) {
		return ConfiguracionCacheUtil.getInstance().getPwrConfUtil(key);
	}
}
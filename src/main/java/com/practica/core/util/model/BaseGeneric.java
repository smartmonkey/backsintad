/**
 * 
 */
package com.practica.core.util.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author desarrollo
 *
 */
public class BaseGeneric implements Serializable {
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La offset. */
	private int offset;
	
	/** La start row. */
	private int startRow;
	
	private String search;
	
	private boolean checked;
	
	private transient Object id;
			
	private String idEntidadSelect = "";
	
	private boolean esEliminado = false;

	private String authToken;
	
	private Date fechaUltimoAcceso;


	
	/**
	 * @return the fechaUltimoAcceso
	 */
	public Date getFechaUltimoAcceso() {
		return fechaUltimoAcceso;
	}

	/**
	 * @param fechaUltimoAcceso the fechaUltimoAcceso to set
	 */
	public void setFechaUltimoAcceso(Date fechaUltimoAcceso) {
		this.fechaUltimoAcceso = fechaUltimoAcceso;
	}

	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @return the esEliminado
	 */
	public boolean isEsEliminado() {
		return esEliminado;
	}

	/**
	 * @param esEliminado the esEliminado to set
	 */
	public void setEsEliminado(boolean esEliminado) {
		this.esEliminado = esEliminado;
	}

	/**
	 * Establece el start row.
	 *
	 * @param startRow el new start row
	 */
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	/**
	 * Obtiene start row.
	 *
	 * @return start row
	 */
	public int getStartRow() {
		return startRow;
	}

	/**
	 * Establece el offset.
	 *
	 * @param offset el new offset
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * Obtiene offset.
	 *
	 * @return offset
	 */
	public int getOffset() {
		return offset;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	
	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	

	public String getIdEntidadSelect() {
		return idEntidadSelect;
	}

	public void setIdEntidadSelect(String idEntidadSelect) {
		this.idEntidadSelect = idEntidadSelect;
	}

}


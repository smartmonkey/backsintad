package com.practica.core.util.security.jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.practica.core.sintad.dao.local.TipoDocumentoDaoLocal;
import com.practica.core.sintad.dao.local.UsuarioDaoLocal;
import com.practica.core.sintad.model.dto.UsuarioDTO;
import com.practica.core.util.factory.TransferDataObjectUtil;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
	
	  @Autowired
	   UsuarioDaoLocal usuarioDaoImpl;
	  @Autowired
	  TipoDocumentoDaoLocal tipoUsuarioDaoImpl;

	  @Override
	  @Transactional
	  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {	  
		UsuarioDTO user = null;
	    try {	    	
	    	 user = TransferDataObjectUtil.transferObjetoEntityDTO(usuarioDaoImpl.validarLogin(email),UsuarioDTO.class); 			
		} catch (UsernameNotFoundException e) {
			// TODO: handle exception
			 new UsernameNotFoundException("User Not Found with username: " + email);
		}
	    
	    
	    return UserDetailsImpl.build(user);
	  }
}

package com.practica.core.util.security.jwt;

import java.util.Collection;
import java.util.Collections;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.practica.core.sintad.model.dto.UsuarioDTO;


import lombok.AllArgsConstructor;


public class UserDetailsImpl implements UserDetails {
	  private static final long serialVersionUID = 1L;

	  private Long id;

	  private String nombre;

	  private String email;

	  @JsonIgnore
	  private String password;
	
	  /**
	 * @param id
	 * @param nombre
	 * @param email
	 * @param password
	 */
	public UserDetailsImpl(Long id, String nombre, String email, String password) {
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.password = password;
	}

	public static UserDetailsImpl build(UsuarioDTO user) {

		  return new UserDetailsImpl(
			        user.getIdUsuario(), 
			        user.getNombre(), 
			        user.getEmail(),
			        user.getPassword());
		  }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return Collections.emptyList();
	}
	
	

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	
	public String getNombre() {
		return nombre;		
	}
	
	public String getEmail() {
		return email;
	}
	
}


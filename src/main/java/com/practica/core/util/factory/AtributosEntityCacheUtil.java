/**
 * 
 */
package com.practica.core.util.factory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.practica.core.util.vo.AtributoEntityVO;
/**
 * @author desarrollo
 *
 */
public class AtributosEntityCacheUtil {

	/** La log. */
	private static Logger log = LoggerFactory.getLogger(AtributosEntityCacheUtil.class);
	
	/** La configurador cache utl. */
	private static AtributosEntityCacheUtil atributoEntityCacheUtl = null;
	
	
   /** El resultado valor variable. */
   private static  Map<String, List<AtributoEntityVO>> entityAtributoMap = new HashMap<>();
   private static  Map<String, Integer> entityAtributoCantidaCamposMap = new HashMap<>();
   
  	/** El flag cargo listado. */
	private boolean flagCargoListado = false;
	
	/**
	 * Instancia un nuevo administracion cache utl.
	 */
	public AtributosEntityCacheUtil() {
		super();
	}
	
	/**
	 * Instanciar.
	 *
	 * @return the configurador cache utl
	 */
	public static AtributosEntityCacheUtil getInstance() {
		if (atributoEntityCacheUtl == null) {
			createInstance();
		} else if (!atributoEntityCacheUtl.isFlagCargoListado()) {
			atributoEntityCacheUtl.sincronizarData();
		}
		return atributoEntityCacheUtl;
	}
	public static AtributosEntityCacheUtil getInstance(String... paquete) {
		if (atributoEntityCacheUtl == null) {
			createInstance(paquete);
		} else if (!atributoEntityCacheUtl.isFlagCargoListado()) {
			atributoEntityCacheUtl.sincronizarData(paquete);
		}
		return atributoEntityCacheUtl;
	}
	 /**
     * Creates the instance.
     */
    private static synchronized void createInstance() {
    	if (atributoEntityCacheUtl == null) {
			atributoEntityCacheUtl = new AtributosEntityCacheUtil();
			atributoEntityCacheUtl.sincronizarData();
		}
    }
    
    private static synchronized void createInstance(String... paquete) {
    	if (atributoEntityCacheUtl == null) {
			atributoEntityCacheUtl = new AtributosEntityCacheUtil();
			atributoEntityCacheUtl.sincronizarData(paquete);
		}
    }

	/**
	 * Sincronizar data.
	 */
	public   void sincronizarData() {
		sincronizarAtributos();
	}
	public   void sincronizarData(String... paquete) {
		sincronizarAtributos(paquete);
	}
	
	/**
	 * Obtener atributos.
	 *
	 * @param <T> el tipo generico
	 * @param alias el alias
	 * @param entityClass el entity class
	 * @param isNative el is native
	 * @return the string
	 */
	public static String obtenerAtributos(String alias, String entityClass, boolean isNative) {
		List<AtributoEntityVO> listaAtributos = entityAtributoMap.get(entityClass);
		return obtenerAtributos(alias, listaAtributos,isNative);
	}
	
	/**
	 * Obtener atributos.
	 *
	 * @param <T> el tipo generico
	 * @param alias el alias
	 * @param listaAtributos el lista atributos
	 * @return the string
	 */
	public static  String obtenerAtributos(String alias, List<AtributoEntityVO> listaAtributos,boolean isNative ) {
		StringBuilder resultado = new StringBuilder();
		int cantidad = listaAtributos.size();
		int contador = 0;
		for (AtributoEntityVO atributoEntityVO : listaAtributos) {
			if (atributoEntityVO.getNombreColumna() != null || atributoEntityVO.isPKCompuesta()) {
				contador++;
				if (alias != null) {
					resultado.append(alias);
					resultado.append(".");
				}
				obtenerAtributos(resultado, atributoEntityVO, isNative);
				if (contador != cantidad) {
					resultado.append(",");
				}
			}
		}
		return resultado.toString();
	}
	public static  StringBuilder obtenerAtributos(StringBuilder resultado, AtributoEntityVO atributoEntityVO,boolean isNative ) {
		if (isNative) {
			if (!atributoEntityVO.isPKCompuesta())  {
				resultado.append(atributoEntityVO.getNombreColumna());
			} else {
				resultado.append(obtenerAtributos(null,atributoEntityVO.getListaAtributoEntityVOPK(),true));
			}
		} else {
			if (!atributoEntityVO.isPKCompuesta())  {
				resultado.append(atributoEntityVO.getNombreAtributo());
			} else {
				resultado.append(obtenerAtributos(null,atributoEntityVO.getListaAtributoEntityVOPK(),true));
			}
		}
		return resultado;
	}
	public static  String obtenerAtributosValues(List<AtributoEntityVO> listaAtributos) {
		StringBuilder resultado = new StringBuilder();
		int cantidad = listaAtributos.size();
		int contador = 0;
		for (AtributoEntityVO atributoEntityVO : listaAtributos) {
			if (!StringUtils.isNullOrEmpty(atributoEntityVO.getNombreColumna()) || atributoEntityVO.isPKCompuesta() ) {
				contador++;
				if (atributoEntityVO.isPKCompuesta())  {
					resultado.append(obtenerAtributosValues(atributoEntityVO.getListaAtributoEntityVOPK()));
				} else {
					resultado.append(":");
					resultado.append(atributoEntityVO.getNombreAtributo());
				}
				if (contador != cantidad) {
					resultado.append(",");
				}
			}
		}
		return resultado.toString();
	}
	
	/**
	 * Obtener lista atributos.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @return the list
	 */
	public static List<AtributoEntityVO> obtenerListaAtributos(String entityClass) {
		return entityAtributoMap.get(entityClass);
	}
	public static  Integer obtenerListaAtributosCantidad(String entityClass) {
		return entityAtributoCantidaCamposMap.get(entityClass);
	}
	/**
	 * Obtener lista atributos.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @return the list
	 */
	public static <T> List<AtributoEntityVO> obtenerListaAtributos(Class<T> entityClass) {
		if (!entityAtributoMap.containsKey(entityClass.getName())){
			try {
				List<AtributoEntityVO> listaAtrivuto = obtenerAtributosColunm(entityClass,true,0);
				entityAtributoMap.put(entityClass.getName(), listaAtrivuto);
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}
			
		} 
		return entityAtributoMap.get(entityClass.getName());
	}
	
	/**
	 * Obtener lista atributos.
	 *
	 * @param <T> the generic type
	 * @param entityClass the entity class
	 * @return the list
	 */
	public static <T> List<AtributoEntityVO> obtenerListaAtributosEntity(Class<T> entityClass) {
		if (!entityAtributoMap.containsKey(entityClass.getName())){
			try {
				List<AtributoEntityVO> listaAtrivuto = obtenerAtributosColunm(entityClass,false,0);
				entityAtributoMap.put(entityClass.getName(), listaAtrivuto);
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}
			
		} 
		return entityAtributoMap.get(entityClass.getName());
	}
	
	
	/**
	 * Prints the fields.
	 *
	 * @param <T> el tipo generico
	 * @param dtoTemp el dto temp
	 * @return the list
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @ the exception
	 */
	private static <T> List<AtributoEntityVO>  obtenerAtributosColunm(Class<T> dtoTemp, boolean isClaseNormal, int posicionTemp) throws InstantiationException, IllegalAccessException  {
		T dto =  dtoTemp.newInstance();
		List<AtributoEntityVO> resultado = new ArrayList<>();
		String tableName = "";
		String schema = "";
		dtoTemp.getDeclaredAnnotations();
		if (dtoTemp.isAnnotationPresent(Table.class) ) {
			Table table = dtoTemp.getAnnotation(Table.class);
			tableName = table.name();
			if (table.schema() != null) {
				schema = table.schema();
			}
		}
		Field[] propiedades = dto.getClass().getDeclaredFields();
		Class<?> current = dtoTemp.getSuperclass();
		while(current  != null && !current.getName().contains("java.lang.Object") ){ // we don't want to process Object.class
		    // do something with current's fields
			List<AtributoEntityVO> listaAtributosHerencia = AtributosEntityCacheUtil.getInstance().obtenerListaAtributos(current);
			if (listaAtributosHerencia != null) {
			    	resultado.addAll(listaAtributosHerencia);
			}
		    current = current.getSuperclass();		   
		}
		addAtributoEntityVO(dto,dtoTemp,isClaseNormal,tableName,schema,posicionTemp, propiedades,resultado);
		return resultado;
	}
	private static  <T> List<AtributoEntityVO> addAtributoEntityVO(T dto,Class<T> dtoTemp,boolean isClaseNormal,String tableName, String schema, int posicionTemp,Field[] propiedades,List<AtributoEntityVO> resultado) {
		int posicion = posicionTemp;
		int cantidadColumna = 0;
		for (Field pd : propiedades) {
			try {
				if (pd.isEnumConstant()) {
					continue;
				}
				Field f = dto.getClass().getDeclaredField(pd.getName());
				if (f != null && ((f.isAnnotationPresent(Column.class) || f.isAnnotationPresent(EmbeddedId.class)) || isClaseNormal)) {
						boolean isColum =  f.isAnnotationPresent(Column.class);
						boolean esPK =  f.isAnnotationPresent(Id.class);
						AtributoEntityVO objAtri = new AtributoEntityVO();
						objAtri.setTableName(tableName);
						objAtri.setSchema(schema);
						objAtri.setColumn(isColum);
						objAtri.setEsPK(esPK);
						if (!f.isAnnotationPresent(EmbeddedId.class)) {
							objAtri.setNombreAtributo(pd.getName());
							objAtri.setClasssAtributo(f.getType());
							objAtri.setClasssAtributoType(f.getType());
							if (f.isAnnotationPresent(Column.class) ) {
								cantidadColumna++;
								Column column = f.getAnnotation(Column.class);
								objAtri.setNombreColumna(column.name());
								entityAtributoCantidaCamposMap.put(dtoTemp.getName(), cantidadColumna);
								objAtri.setPosicion(posicion);
								posicion ++;
							}
							resultado.add(objAtri);							
					} else {
						objAtri.setListaAtributoEntityVOPK(obtenerAtributosColunm(pd.getType(), isClaseNormal,0));
						objAtri.setPKCompuesta(true);
						objAtri.setNombreAtributo(pd.getName());
						objAtri.setClasssAtributo(f.getType());
						objAtri.setClasssAtributoType(f.getType());
						objAtri.setPosicion(posicion);
						resultado.add(objAtri);
					}
				}
			} catch (Exception e) {
				continue;
			}
		}
		return resultado;
	}
	
	/**
	 * Sincronizar obtener listados.
	 *
	 * @param <T> el tipo generico
	 */
	private  <T> void sincronizarAtributos() {
		try {
			List<Object> listEntity = new ArrayList<>();
			entityAtributoMap = new HashMap<>();
			for (Object object : listEntity) {
				Class<T> classs = (Class<T>) object;
				List<AtributoEntityVO> listaAtrivuto = obtenerAtributosColunm(classs,false,0);
				entityAtributoMap.put(classs.getName(), listaAtrivuto);
			}
				
			flagCargoListado = true;
			sincronizarAtributosVO();
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			flagCargoListado = false;
		}
		
	}
	private  <T> void sincronizarAtributos(String... paquete) {
		try {
			List<Object> listEntity = obtenerClasesReflections(paquete);
			for (Object object : listEntity) {
				Class<T> classs = (Class<T>) object;
				List<AtributoEntityVO> listaAtrivuto = obtenerAtributosColunm(classs,false,0);
				entityAtributoMap.put(classs.getName(), listaAtrivuto);
			}
			flagCargoListado = true;
			sincronizarAtributosVO();
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			flagCargoListado = false;
		}
		
	}
	private static List<Object> obtenerClasesReflections(String... paquete) {
		List<Object> resultado = new ArrayList<>();
		try {
			List<ClassLoader> classLoadersList = new LinkedList<>();
			classLoadersList.add(ClasspathHelper.contextClassLoader());
			classLoadersList.add(ClasspathHelper.staticClassLoader());
			for (String classLoaderPaquete : paquete) {
				try {
					 Reflections modules = new Reflections(classLoaderPaquete);
					 Reflections reflections = new Reflections(new ConfigurationBuilder()
				    .setScanners(new SubTypesScanner(false), new ResourcesScanner())
				    .setUrls(modules.getConfiguration().getUrls())
				    .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(classLoaderPaquete))));
					Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
					for (Class<?> class1 : classes) {
						resultado.add(class1);
					}
				} catch (Exception e) {
					log.error("No cargo para el paquete " + paquete  +" --> " + classLoaderPaquete);
					//log.error(e.getMessage(),e);
				}
				
			}
			
		} catch (Exception e) {
			log.error("error al cargar el paquete " + e.getMessage());
		}
		return resultado;
	}
	
	private  <T> void sincronizarAtributosVO() {
		try {
			List<Object> listVo = new ArrayList<>();
			for (Object object : listVo) {
				Class<T> classs = (Class<T>) object;
				List<AtributoEntityVO> listaAtrivuto = obtenerAtributosColunm(classs,true,0);
				entityAtributoMap.put(classs.getName(), listaAtrivuto);
			}
			flagCargoListado = true;
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			flagCargoListado = false;
		}
	}
	
	
	/**
	 * Comprueba si es flag cargo listado.
	 *
	 * @return true, si es flag cargo listado
	 */
	public boolean isFlagCargoListado() {
		return flagCargoListado;
	}

	/**
	 * Establece el flag cargo listado.
	 *
	 * @param flagCargoListado el new flag cargo listado
	 */
	public void setFlagCargoListado(boolean flagCargoListado) {
		this.flagCargoListado = flagCargoListado;
	}

	/**
	 * Obtiene entity atributo map.
	 *
	 * @return entity atributo map
	 */
	public static Map<String, List<AtributoEntityVO>> getEntityAtributoMap() {
		return entityAtributoMap;
	}

	 
}
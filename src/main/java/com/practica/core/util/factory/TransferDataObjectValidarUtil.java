/**
 * 
 */
package com.practica.core.util.factory;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.practica.core.util.model.ConfiguracionTramaDetalleDTO;
import com.practica.core.util.model.RespuestaNaturalType;
import com.practica.core.util.vo.AtributoEntityVO;
import com.practica.core.util.vo.ConfiguracionUtil;
import com.practica.core.util.vo.ConstanteConfiguracionTramaUtil;
import com.practica.core.util.vo.FechaUtil;
import com.practica.core.util.vo.ResourceUtil;
import com.practica.core.util.vo.ValueDataVO;

/**
 * @author desarrollo
 *
 */
public class TransferDataObjectValidarUtil   implements Serializable {
	private static final String SEPARADOR = ";";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** La Constante ARTIFICIO_CLASS. */
	public static final String ARTIFICIO_CLASS = "$class";

	/** La Constante CARACTER_EXTRANHO. */
	public static final String CARACTER_EXTRANHO = "ï»¿";
	
	public static void defaultLocaleProcess(){
		Locale.setDefault(Locale.US);
	}
	
	public static <T> Field fieldHerenciaSet(T resultado,Map<String,Integer> fieldHerenciaMap,AtributoEntityVO objAtr) throws NoSuchFieldException  {
		Field f = null;
		if (!fieldHerenciaMap.containsKey(objAtr.getNombreAtributo())) {
			f = resultado.getClass().getDeclaredField(objAtr.getNombreAtributo());
		} else {
			int nivelHerencia = fieldHerenciaMap.get(objAtr.getNombreAtributo());
			int nivel = 0;
			Class<?> currentHerencia = resultado.getClass();
			while(currentHerencia.getSuperclass() != null){ 
				nivel++;
				currentHerencia = currentHerencia.getSuperclass();
				if (nivel == nivelHerencia) {
					f = currentHerencia.getDeclaredField(objAtr.getNombreAtributo());
					break;
				}
			}
		}		
		return f;
	}
	public static <T> Map<String,Integer> fieldHerenciaMap(T resultado) {
		Map<String,Integer> fieldHerenciaMap = new HashMap<>();
		Class<?> current = resultado.getClass();
		int nivel = 0;
		while(current.getSuperclass() != null){ // we don't want to process Object.class
		    // do something with current's fields
		    current = current.getSuperclass();
		    List<AtributoEntityVO> listaAtributosHerencia = AtributosEntityCacheUtil.getInstance().obtenerListaAtributos(current);
		    nivel++;
		    if (listaAtributosHerencia != null) {
			    for (AtributoEntityVO field : listaAtributosHerencia) {
			    	fieldHerenciaMap.put(field.getNombreAtributo(), nivel);
				}
		    }
		}
		return fieldHerenciaMap;
	}
	public static String obtenerHandlerHibernate(String className) {
		String resultado = "";
		String handlerHibernateKeys = ConfiguracionUtil.getPwrConfUtil("handlerHibernate.impl");
		boolean existeConfiguracion = false;
		if (!StringUtils.isNullOrEmpty(handlerHibernateKeys)) {
			String[] dataKey = handlerHibernateKeys.split(",", -1);
			if (dataKey != null && dataKey.length > 0) {
				for (String handlerHibernate : dataKey) {
					if (className.contains(handlerHibernate)) {
						resultado = handlerHibernate;
						existeConfiguracion =  true;
						break;
					}
				}
			}
		} 
		if (!existeConfiguracion) {
			resultado = "_$$_javassist";
			if (!className.contains("_$$_javassist")) {
				resultado = "$HibernateProxy";
			}
		}
		return resultado;
		
	}
	
	
	public static boolean validarCSV(String[] data,Map<String,Object> campoMappingCVSMap) {
		boolean resultado = false;
		for (Map.Entry<String, Object> objAtr : campoMappingCVSMap.entrySet()) {
	         int index = Integer.parseInt(objAtr.getValue() + "");			
			 try {
				String hssfCell = data[index];
				if (hssfCell.contains(CARACTER_EXTRANHO)) {
					hssfCell = hssfCell.substring(lengthCaracterExtranho());
				}
				if (hssfCell != null && !hssfCell.trim().equals("")) {
					resultado = true;
					break;
				}
			} catch (Exception e) {
			}
		}
		return resultado;
	}
	public static int lengthCaracterExtranho() {
		return CARACTER_EXTRANHO.length() + 1;
	}
	protected static StringBuilder generarKeyAgrupador(Map<String,ValueDataVO> resultadoTemp,Map<String,ConfiguracionTramaDetalleDTO> configuracionTramaDetalleMap) {
    	StringBuilder key = new StringBuilder();
		for (Map.Entry<String, ValueDataVO> mapValue : resultadoTemp.entrySet()) {
			// validar
			ConfiguracionTramaDetalleDTO configuracionTramaDetalle = configuracionTramaDetalleMap.get(mapValue.getKey());
			boolean isAgrupador = RespuestaNaturalType.SI.getKey().equals(configuracionTramaDetalle.getFlagCampoAgrupador());
			if (isAgrupador) {
				key.append(mapValue.getValue() + "");
			}
		}
		return key;
    }
    protected static boolean validarTXT(String data,Map<String,Object> campoMappingTXTMap) {
		boolean resultado = false;
		for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
			String index = objAtr.getValue() + "";
			try {
				String[] begin = index.split(SEPARADOR);
				int beginIndex = Integer.parseInt(begin[0]);
				if (beginIndex < 0 ) {
					continue;
				}
				int endIndex = Integer.parseInt(begin[1]);
				if (endIndex < 0 ) {
					continue;
				}
				//Inicio aumentado el nuevo requerimiento
				if (data.length() < endIndex) {
					endIndex = data.length();
				}
				//Fin aumentado el nuevo requerimiento
				String hssfCell =  data.substring(beginIndex, endIndex);
				if (hssfCell.contains(CARACTER_EXTRANHO)) {
					hssfCell = hssfCell.substring(lengthCaracterExtranho());
				}
				if (hssfCell != null && !hssfCell.trim().equals("")) {
					resultado = true;
					break;
				}
			} catch (Exception e) {
				resultado = false;
			}
		}
		
		return resultado;
	}
    
    protected static boolean validarTXTCoordenada(String data,Map<String,Object> campoMappingTXTMap) {
		boolean resultado = false;
		for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
			String index = objAtr.getValue() + "";
			int filaData = 0;
			try {
				String[] begin = index.split(SEPARADOR);
				filaData = Integer.parseInt(begin[0]);
				if (filaData < 0 ) {
					continue;
				}
				int beginIndex = Integer.parseInt(begin[1]);
				if (beginIndex < 0 ) {
					continue;
				}
				int endIndex = Integer.parseInt(begin[2]);
				if (endIndex < 0 ) {
					continue;
				}
				//Inicio aumentado el nuevo requerimiento
				if (data.length() < endIndex) {
					endIndex = data.length();
				}
				//Fin aumentado el nuevo requerimiento
				String hssfCell = data.substring(beginIndex, endIndex);
				if (hssfCell.contains(CARACTER_EXTRANHO)) {
					hssfCell = hssfCell.substring(lengthCaracterExtranho());
				}
				if (hssfCell != null && !hssfCell.trim().equals("")) {
					resultado = true;
					break;
				}
			} catch (Exception e) {
				resultado = false;
			}
		}
		return resultado;
	}
   
	/**
	 * Obtener value csv.
	 *
	 * @param hssfRow el hssf row
	 * @param index el index
	 * @param objAt el obj at
	 * @param formatoFecha el formato fecha
	 * @param filaData el fila data
	 * @param parametroMap el parametro map
	 * @return the value data vo
	 */
	protected static ValueDataVO obtenerValueCSV(String[] hssfRow,int index,String objAt,String formatoFecha,int filaData,Map<String,Object> parametroMap) {
		ValueDataVO resultado = new ValueDataVO();
		resultado.setFila("" + (filaData));
		if (index < 0) {
			return resultado;
		}
		String hssfCell = null;
		try {
			hssfCell = (String) hssfRow[index];
			if (hssfCell.contains(CARACTER_EXTRANHO)) {
				hssfCell = hssfCell.substring(lengthCaracterExtranho());
			}
		} catch (Exception e) {
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				resultado.setData("${ERROR} al obtener la información, la posición " + (index + 1) +  " no existe en la fila ");
    	    } else {
    			//log.error("no existe index " +  (index + 1) + " " + e.toString());
				resultado.setData("${ERROR}:Posición(" + (index + 1) + ") no existe en la fila : " + (filaData));
    	    }
		}
		try {
			if (hssfCell != null && !hssfCell.toString().trim().equals("")) {
				resultado = obtenerValueParse(hssfCell.toString(), objAt,formatoFecha,filaData,parametroMap);
			}
		} catch (Exception e) {
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				resultado.setData("${ERROR} al obtener la información, la posición " + (index + 1) +  " no existe en la fila ");
    	    } else {
				resultado.setData("${ERROR}:Posición(" + (index + 1) + ") no existe  en la fila : " + (filaData));
    	    }
		}
		return resultado;
	}
	/**
	 * Obtener value posicion.
	 *
	 * @param data el data
	 * @param index el index
	 * @param objAt el obj at
	 * @param formatoFecha el formato fecha
	 * @param filaData el fila data
	 * @return the value data vo
	 */
	protected static ValueDataVO obtenerValuePosicion(String data,String index,String objAt,String formatoFecha, int filaData,Map<String, Object> parametroMap) {
		ValueDataVO resultado = new ValueDataVO();
		resultado.setFila((filaData) + "");
		String hssfCell = null;
		String vIindex = "";
		try {
			String[] begin = index.split(SEPARADOR);
			int beginIndex = Integer.parseInt(begin[0]);
			if (beginIndex < 0 ) {
				return resultado;
			}
			vIindex = "" + (beginIndex + 1);
			int endIndex = Integer.parseInt(begin[1]);
			if (endIndex < 0 ) {
				return resultado;
			}
			vIindex = vIindex + SEPARADOR + (endIndex + 1);
			//Inicio aumentado el nuevo requerimiento
			if (data.length() < endIndex) {
				endIndex = data.length();
			}
			//Fin aumentado el nuevo requerimiento
			hssfCell = data.substring(beginIndex, endIndex);
			if (hssfCell.contains(CARACTER_EXTRANHO)) {
				hssfCell = hssfCell.substring(lengthCaracterExtranho());
			}
		} catch (Exception e) {
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				resultado.setData("${ERROR} al obtener la información, la posición " + recortarCadenaValuePosicion(vIindex) +  " no existe en la fila ");
			} else {
				resultado.setData("${ERROR}:Posición(" + (vIindex) + ") no existe en la fila " + (filaData));				
			}
		}
		try {
			if (hssfCell != null && !hssfCell.trim().equals("")) {
				resultado = obtenerValueParse(hssfCell, objAt,formatoFecha,filaData,parametroMap);
			}
		} catch (Exception e) {
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				resultado.setData("${ERROR} al obtener la información, la posición " + recortarCadenaValuePosicion(vIindex) +  " no existe en la fila ");
			} else {
				resultado.setData("${ERROR}:Posición(" + (index + 1) + ") no existe en la fila " + (filaData));				
			}
			
		}
		return resultado;
	}
	
	
	/**
	 * Recortar cadena personalizado.
	 *
	 * @param vIindex el v iindex
	 * @return the string
	 */
	public static StringBuilder recortarCadenaValuePosicion(String vIindex) {
		StringBuilder resultado = new StringBuilder();
		resultado.append(vIindex.substring(0,((vIindex.indexOf(SEPARADOR))) > 0 ? (vIindex.indexOf(SEPARADOR)) : 0));
		return resultado;
	}
	
	/**
	 * Obtener coordenada valor.
	 *
	 * @param dataList el data list
	 * @param dataValue el data value
	 * @param index el index
	 * @param objAt el obj at
	 * @param formatoFecha el formato fecha
	 * @param isCabecera el is cabecera
	 * @return the value data vo
	 */
	protected static ValueDataVO obtenerCoordenadaValor(List<String> dataList, String dataValue,String index,String objAt,String formatoFecha, boolean isCabecera,Map<String, Object> parametroMap) {
		ValueDataVO resultado = new ValueDataVO();
		String hssfCell = null;
		int filaData = 0;
		String vIindex = "";
		try {
			String[] begin = index.split(SEPARADOR);
			filaData = Integer.parseInt(begin[0]);
			if (filaData < 0 ) {
				return resultado;
			}
			vIindex = "" + (filaData);
			int beginIndex = Integer.parseInt(begin[1]);
			if (beginIndex < 0 ) {
				return resultado;
			}
			vIindex = vIindex + SEPARADOR + (beginIndex + 1);
			int endIndex = Integer.parseInt(begin[2]);
			if (endIndex < 0 ) {
				return resultado;
			}
			vIindex = vIindex + SEPARADOR + (endIndex + 1);
			if (isCabecera) {
				if (dataList.size() >= filaData) {
					String data = dataList.get(filaData);
					//Inicio aumentado el nuevo requerimiento
					if (data.length() < endIndex) {
						endIndex = data.length();
					}
					//Fin aumentado el nuevo requerimiento
					hssfCell = data.substring(beginIndex, endIndex);
					if (hssfCell.contains(CARACTER_EXTRANHO)) {
						hssfCell = hssfCell.substring(lengthCaracterExtranho());
					}
				}
				
			} else {
				//Inicio aumentado el nuevo requerimiento
				if (dataValue.length() < endIndex) {
					endIndex = dataValue.length();
				}
				//Fin aumentado el nuevo requerimiento
				hssfCell =  dataValue.substring(beginIndex, endIndex);
				if (hssfCell.contains(CARACTER_EXTRANHO)) {
					hssfCell = hssfCell.substring(lengthCaracterExtranho());
				}
			}
			resultado.setFila((filaData + 1) + "");   //SE SUMA + 1 PARA PRESENTAR AL USUARIO 
			
		} catch (Exception e) {
			resultado.setFila((filaData + 1) + ""); //SE SUMA + 1 PARA PRESENTAR AL USUARIO 
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				resultado.setData("${ERROR} al obtener la información, la posición " + recortarCadenaCoordenadaValor(vIindex) +  " no existe en la fila ");
			} else {
				resultado.setData("${ERROR}:Posición(" + (vIindex) + ") no existe en la fila " + (filaData + 1));				
			}
		}
		try {
			if (hssfCell != null && !hssfCell.trim().equals("")) {
				resultado = obtenerValueParse(hssfCell, objAt,formatoFecha,filaData,parametroMap);
			}
		} catch (Exception e) {
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				resultado.setData("${ERROR} al obtener la información, la posición " + recortarCadenaCoordenadaValor(vIindex) +  " no existe en la fila ");
			} else {
				resultado.setData("${ERROR}:Posición(" + (index + 1) + ") no existe en la fila " + (filaData));				
			}
			
		}
		return resultado;
	}
	
	
	/**
	 * Recortar cadena coordenada valor.
	 *
	 * @param vIindex el v iindex
	 * @return the string builder
	 */
	public static StringBuilder recortarCadenaCoordenadaValor(String vIindex) {
		StringBuilder resultado = new StringBuilder();
		int index1 = vIindex.indexOf(SEPARADOR);
		int index2 = vIindex.indexOf(SEPARADOR, index1 + 1);
		resultado.append(vIindex.substring(index1 + 1 , index2));
		return resultado;
	}
	
	
	
	/**
	 * Obtener value parse.
	 *
	 * @param resultadoValor el resultado valor
	 * @param objAt el obj at
	 * @param formatoFecha el formato fecha
	 * @param filaData el fila data
	 * @return the object
	 * @ the exception
	 */
	public static ValueDataVO obtenerValueParse(String resultadoValor,String objAt, String formatoFecha, int filaData,Map<String, Object> parametroMap)  {
		ValueDataVO resultado = new ValueDataVO();
		resultado.setFila("" + (filaData != 0 ? filaData : "") );
		try {
			if (StringUtils.isNullOrEmpty(resultadoValor))  {
				return null;
			}
			resultadoValor = StringUtils.quitarCaracterExtranio(resultadoValor);
			resultadoValor = resultadoValor.trim();
			if (objAt.equals(Boolean.class.getName())) {
				resultado.setData(Boolean.valueOf(resultadoValor));
			} else if (objAt.equals(Integer.class.getName())) {
				resultado.setData(Integer.parseInt(resultadoValor));
			} else if (objAt.equals(Float.class.getName())) {
				resultado.setData(Float.parseFloat(resultadoValor));
			} else if (objAt.equals(Double.class.getName())) {
				resultado.setData(Double.parseDouble(resultadoValor));
			} else if (objAt.equals(Long.class.getName())) {
				resultado.setData(Long.parseLong(resultadoValor));
			} else if (objAt.equals(BigDecimal.class.getName())) {
				//convirtiendo datos numericos fomateados
				resultadoValor = reingenieriaFormateoNumerico(resultadoValor);
				resultado.setData(new  BigDecimal(resultadoValor));
			} else if (objAt.equals(Character.class.getName())) {
				resultado.setData(resultadoValor.charAt(0));
			}  else if (objAt.equals(Date.class.getName())) {
				resultado.setData(FechaUtil.obtenerFechaFormatoPersonalizado(resultadoValor,formatoFecha));
			}  else if (objAt.equals(Collection.class.getName())) {
				resultado =  null;
			}  else  {
				resultado.setData(resultadoValor);
			}
		} catch (Exception e) {
			
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				resultado.setData("${ERROR}${type} El tipo de dato del campo no corresponde a lo configurado");//+ resultadoValor + "
			} else {				
				resultado.setData("${ERROR} :Tipo de Conversión no valido ${type} --> " + resultadoValor + " en la fila " + (filaData));
			}
			return resultado;
		}
		return resultado;
	}
	
	/**
	 * Reingenieria formateo numerico.
	 *
	 * @param dataResul el data resul
	 * @return the string
	 */
	public static String reingenieriaFormateoNumerico(String dataResul) {
		try {
			String formateoDecimal = ConfiguracionUtil.getPwrConfUtil(ConfiguracionUtil.CARACTER_FORMATEO_NUMERICO);
			if ( formateoDecimal != null) {
				for (Character chart : formateoDecimal.toCharArray()) {
					dataResul = dataResul.replace(chart + "", "");
				}
			}
		} catch (Exception e) {
		}
		if (dataResul != null) {
			dataResul = dataResul.trim();
		}
		return dataResul;
	}
	
	/**
	 * Obtener valor.
	 *
	 * @param resultadoValor el resultado valor
	 * @param objAt el obj at
	 * @param isVO el is vo
	 * @return the object
	 * @ the exception
	 */
	public static Object obtenerValor(String resultadoValor,AtributoEntityVO objAt,boolean isVO)  {
		return obtenerValor(resultadoValor,objAt,isVO, new HashMap<>());
	}
	
	/**
	 * Obtener valor.
	 *
	 * @param resultadoValor el resultado valor
	 * @param objAt el obj at
	 * @param isVO el is vo
	 * @param formatoMap el formato map
	 * @return the object
	 * @ the exception
	 */
	public static Object obtenerValor(String resultadoValor,AtributoEntityVO objAt,boolean isVO,Map<String,String> formatoMap)  {
		Object resultado = null;
		if (StringUtils.isNullOrEmpty(resultadoValor))  {
			return null;
		}
		try {
			if ("id".equalsIgnoreCase(objAt.getNombreAtributo())) {//comodin
				resultado =  resultadoValor;
			} else if ("idPadreView".equalsIgnoreCase(objAt.getNombreAtributo())) {//comodin
				resultado =  resultadoValor;
			} else if (objAt.getClasssAtributoType().isAssignableFrom(Boolean.class)) {
				resultado = Boolean.valueOf(resultadoValor);
			} else if (objAt.getClasssAtributoType().isAssignableFrom(int.class)) {
				resultado = Integer.parseInt(resultadoValor);
			} else if (objAt.getClasssAtributoType().isAssignableFrom(Integer.class)) {
				resultado =  Integer.parseInt(resultadoValor);
			} else if (objAt.getClasssAtributoType().isAssignableFrom(Float.class)) {
				resultado =  Float.parseFloat(resultadoValor);
			} else if (objAt.getClasssAtributoType().isAssignableFrom(Double.class)) {
				resultado =  Double.parseDouble(resultadoValor);
			} else if (objAt.getClasssAtributoType().isAssignableFrom(Long.class)) {
				resultado =  Long.parseLong(resultadoValor);
			} else if (objAt.getClasssAtributoType().isAssignableFrom(BigDecimal.class)) {
				resultado =  new  BigDecimal(resultadoValor);
			} else if (objAt.getClasssAtributoType().isAssignableFrom(Character.class)) {
				resultado =  resultadoValor.charAt(0);
			}  else if (objAt.getClasssAtributoType().isAssignableFrom(Date.class)) {
				if (!isVO) {
					if (formatoMap.containsKey(objAt.getNombreAtributo())) {
						resultado =  FechaUtil.obtenerFechaFormatoPersonalizado(resultadoValor, formatoMap.get(objAt.getNombreAtributo()));
					} else {
						resultado =  FechaUtil.obtenerFecha(resultadoValor);
					}
					
				} else {
					if (formatoMap.containsKey(objAt.getNombreAtributo())) {
						resultado =  FechaUtil.obtenerFechaFormatoPersonalizado(resultadoValor, formatoMap.get(objAt.getNombreAtributo()));
					} else {
						resultado = FechaUtil.obtenerFechaFormatoCompleto(resultadoValor);
					}
				}
			}  else if (objAt.getClasssAtributoType().isAssignableFrom(Collection.class)) {
				resultado =  null;
			} else  {
				resultado =  resultadoValor;
			}
		} catch (Exception e) {
			resultado =  null;
		}
		
		return resultado;
	}
	
}
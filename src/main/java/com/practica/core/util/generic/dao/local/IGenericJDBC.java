package com.practica.core.util.generic.dao.local;

import java.util.List;
import java.util.Map;

import com.practica.core.util.model.ScriptSqlResulJDBCVO;

public interface IGenericJDBC {
	/**
	 * Execute query.
	 *
	 * @param <T>               el tipo generico
	 * @param sql               el sql
	 * @param parametros        el parametros
	 * @param entityClassEntity el entity class entity
	 * @return the t @ the exception
	 */
	<T> T executeQuery(StringBuilder sql, Map<String, Object> parametros, Class<T> entityClassEntity);

	/**
	 * Execute query.
	 *
	 * @param sp          el sql
	 * @param parametros  el parametros
	 * @param isProcedure el is procedure
	 * @return the script sql resul jdbcvo @ the exception
	 */
	ScriptSqlResulJDBCVO executeQuerySP(StringBuilder sp);

	ScriptSqlResulJDBCVO executeQuerySP(String sp);

	/**
	 * Execute query.
	 *
	 * @param sql              el sql
	 * @param parametros       el parametros
	 * @param isProcedure      el is procedure
	 * @param parametroOutType el parametro out type
	 * @param parametroInType  el parametro in type
	 * @return the script sql resul jdbcvo @ the exception
	 */
	ScriptSqlResulJDBCVO executeQuerySP(StringBuilder sql, String jNDIConexion, List<Integer> parametroOutType,
			List<Object> parametroInType);

	ScriptSqlResulJDBCVO executeQuerySP(String sp, String jNDIConexion, List<Integer> parametroOutType,
			List<Object> parametroInType);

	/**
	 * Execute query.
	 *
	 * @param <T>               el tipo generico
	 * @param sql               el sql
	 * @param parametros        el parametros
	 * @param jNDIConexion      el JNDI conexion
	 * @param entityClassEntity el entity class entity
	 * @return the t @ the exception
	 */
	<T> T executeQuery(StringBuilder sql, Map<String, Object> parametros, String jNDIConexion,
			Class<T> entityClassEntity);

	/**
	 * Execute query list.
	 *
	 * @param <T>               el tipo generico
	 * @param sql               el sql
	 * @param parametros        el parametros
	 * @param entityClassEntity el entity class entity
	 * @return the list @ the exception
	 */
	<T> List<T> executeQueryList(StringBuilder sql, Map<String, Object> parametros, Class<T> entityClassEntity);

	/**
	 * Execute query list.
	 *
	 * @param <T>               el tipo generico
	 * @param sql               el sql
	 * @param parametros        el parametros
	 * @param entityClassEntity el entity class entity
	 * @param formatoMap        el formato map
	 * @return the list @ the exception
	 */
	<T> List<T> executeQueryList(StringBuilder sql, Map<String, Object> parametros, Class<T> entityClassEntity,
			Map<String, String> formatoMap);

	/**
	 * Execute query list.
	 *
	 * @param sql        el sql
	 * @param parametros el parametros
	 * @return the list @ the exception
	 */
	List<Object[]> executeQueryList(StringBuilder sql, Map<String, Object> parametros);

	/**
	 * Execute query list.
	 *
	 * @param <T>               el tipo generico
	 * @param sql               el sql
	 * @param parametros        el parametros
	 * @param jNDIConexion      el JNDI conexion
	 * @param entityClassEntity el entity class entity
	 * @return the list @ the exception
	 */
	<T> List<T> executeQueryList(StringBuilder sql, Map<String, Object> parametros, String jNDIConexion,
			Class<T> entityClassEntity);

	/**
	 * Execute query.
	 *
	 * @param sql        el sql
	 * @param parametros el parametros
	 * @return the script sql resul jdbcvo @ the exception
	 */
	ScriptSqlResulJDBCVO executeQuery(StringBuilder sql, Map<String, Object> parametros);

	/**
	 * Execute query.
	 *
	 * @param sql          el sql
	 * @param parametros   el parametros
	 * @param jNDIConexion el JNDI conexion
	 * @return the script sql resul jdbcvo @ the exception
	 */
	ScriptSqlResulJDBCVO executeQuery(StringBuilder sql, Map<String, Object> parametros, String jNDIConexion);

	/**
	 * Execute update.
	 *
	 * @param sql        el sql
	 * @param parametros el parametros
	 * @return the script sql resul jdbcvo @ the exception
	 */
	ScriptSqlResulJDBCVO executeUpdate(StringBuilder sql, Map<String, Object> parametros);

	/**
	 * Execute update.
	 *
	 * @param sql          el sql
	 * @param parametros   el parametros
	 * @param jNDIConexion el JNDI conexion
	 * @return the script sql resul jdbcvo @ the exception
	 */
	ScriptSqlResulJDBCVO executeUpdate(StringBuilder sql, Map<String, Object> parametros, String jNDIConexion);

	ScriptSqlResulJDBCVO executeQueryPreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			String jNDIConexion);

	ScriptSqlResulJDBCVO executeQueryPreparedStatement(StringBuilder sql, Map<String, Object> parametros);

	ScriptSqlResulJDBCVO executeUpdatePreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			String jNDIConexion);

	ScriptSqlResulJDBCVO executeUpdatePreparedStatement(StringBuilder sql, Map<String, Object> parametros);

}
package com.practica.core.util.generic.rest;

public final class AppHttpHeaderNames {
	
	private AppHttpHeaderNames() {
	    throw new IllegalStateException("Utility AppHttpHeaderNames class");
	}


	 public static final String SERVICE_KEY = "service_key";
	 public static final String AUTH_TOKEN = "auth_token";
	 public static final String VERSION = "version";
	 public static final String ORIGIN  = "Origin";
	 public static final String SERVICE_KEY_VALUE  = "3b91cab8-926f-49b6-ba00-920bcf934c2a";
	 public static final String VERSION_VALUE = "1.0";
	 
}

package com.practica.core.util.generic.rest;


import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.web.context.request.WebRequest;

import com.practica.core.util.factory.TransferDataObjectUtil;
import com.practica.core.util.vo.ResultadoRestVO;

public class GenericServiceRestImpl {
	   
	   
	public static String testNodos() {
		String resultado = "";
		try {
			String nodeName = System.getProperty("jboss.node.name");
			if (nodeName == null) {
				nodeName = InetAddress.getLocalHost().getHostName() + " >>";
			}
			resultado = nodeName;
		} catch (Exception e) {
			resultado = e.getMessage();
		}
		return resultado;
	}
	
	public <T> ResultadoRestVO<T> inicializar(ResultadoRestVO<T> resultado)
	{
		resultado.setCodigoError("");
		resultado.setError(false);
		resultado.setMensajeError("");
		resultado.setListaResultado(new ArrayList<>());
		return resultado;
	}
	 protected <T> ResultadoRestVO<T> parsearResultadoError(Exception e,ResultadoRestVO<T> resultado) {
    	resultado.setError(true);
		resultado.setCodigoError(e.getLocalizedMessage());
		resultado.setMensajeError(e.getMessage() +" --> " + e.toString());
		return resultado;
    }
	 
	 protected Map<String,Object> transferUriInfoMap(WebRequest info) {
			return TransferDataObjectUtil.transferObjetoEntityGetRestMap(info);
	}
}

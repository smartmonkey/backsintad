/**
 * 
 */
package com.practica.core.util.generic.dao.impl;

/**
 * @author desarrollo
 *
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.practica.core.util.factory.CollectionUtil;
import com.practica.core.util.factory.ObjectUtil;
import com.practica.core.util.factory.StringUtils;

public class GenericOperacionDAOImpl {

	
	private static final String NOT_IN = " not in ";

	public Map<String,Object> obtenerParametroListaIn(String nombreParametro,List<?> listaParametroTemp) {
		Map<String,Object> parametraMap = new HashMap<>();
		 int indexDinamic = 0;
		 List<?> listaParametro = new ArrayList<>(listaParametroTemp);
		 if (!listaParametro.isEmpty()) {
			 if (listaParametro.size() > 1000) { 
			      while (listaParametro.size() > 1000) {
			        List<?> subList = new ArrayList<>(listaParametro.subList(0, 1000)) ;
			        parametraMap.put(nombreParametro + indexDinamic, subList);
			        listaParametro.subList(0, 1000).clear();
			        indexDinamic++;
			      }
			  }
			 parametraMap.put(nombreParametro, listaParametro);
		 }
		 
		return parametraMap;
	}
	
	/**
	 * Obtener parametro sql lista in.
	 *
	 * @param nombreParametro the nombre parametro
	 * @param campo the campo
	 * @param listaKeysTemp the lista parametro temp
	 * @param isIn the is in
	 * @return the string builder
	 */
	public StringBuilder obtenerParametroSqlListaIn(String nombreParametro,String campo,List<?> listaKeysTemp, boolean isIn) {
		StringBuilder jpaql = new StringBuilder();
		 int indexDinamic = 0;
		 List<Object> listaParametro = new ArrayList<>(listaKeysTemp);
		 if (!listaParametro.isEmpty()) {
			 jpaql.append(" and ( ");
			 if (listaParametro.size() > 1000) { 
			      while (listaParametro.size() > 1000) {
			        jpaql.append(" " + campo + " " + ( isIn ? "in" : NOT_IN ) + " (:" + nombreParametro + "" + indexDinamic + ") OR ");
			        listaParametro.subList(0, 1000).clear();
			        indexDinamic++;
			      }
			  }
			 jpaql.append("  " + campo + " " + ( isIn ? "in" : NOT_IN ) + "  (:" + nombreParametro + ") ) ");
		 }
		 return jpaql;
	}
	
	public StringBuilder obtenerParametroSqlListaInJdbc(String campo, List<Object> listaParametroTemp, boolean isIn) {
		StringBuilder jpaql = new StringBuilder();
		StringBuilder  cadena = new StringBuilder();
		List<?> listaParametro = new ArrayList<>(listaParametroTemp);
		if (!listaParametro.isEmpty()) {
			jpaql.append(" and ( ");
			while (!listaParametro.isEmpty()) {
				List<?> subList = null;
				if (listaParametro.size() > 1000) {
					subList = new ArrayList<>(listaParametro.subList(0, 1000));
				} else {
					subList = new ArrayList<>(listaParametro.subList(0, listaParametro.size()));
				}
				obtenerConcatenacion(cadena, subList);
				jpaql.append(" " + campo + " " + (isIn ? "in" : NOT_IN) + " (" + cadena + ") ");
				listaParametro.subList(0, subList.size()).clear();
				if (listaParametroTemp.size() > 1000 && !listaParametro.isEmpty()) {
					jpaql.append(" OR ");
				}
			}
			jpaql.append(" )");
		}
		return jpaql;
	}
	
	private StringBuilder obtenerConcatenacion(StringBuilder  cadena ,List<?> subList ) {
		for (Object object : subList) {
			if (!cadena.toString().isEmpty()) {
				cadena.append(", " + object.toString());
			} else {
				cadena = new StringBuilder(object.toString());
			}
		}
		return cadena;
	}
	
	public Map<String,String> obtenerResultadoMap(List<Object[]> listaObjetos, int cantidadKey,int posicionValue) {
		Map<String, String> resultado = new HashMap<>();
		if (!CollectionUtil.isEmpty(listaObjetos)) {
			for (Object[] objects : listaObjetos) {
				String key = StringUtils.generarKey(objects, cantidadKey);
				if (!resultado.containsKey(key)) {
					resultado.put(key, ObjectUtil.objectToString(objects[posicionValue]));
				}
			}
		}
		return resultado;
	}
}
/**
 * 
 */
package com.practica.core.util.generic.dao.impl;

import java.io.Serializable;
import java.util.Map;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceContextType;
import jakarta.persistence.Query;


/**
 * @author desarrollo
 *
 */
public abstract  class  GenericDAOImpl<K,T extends Serializable > extends  GenericOperacionDAOImpl {
	//http://www.baeldung.com/simplifying-the-data-access-layer-with-spring-and-java-generics
	//http://www.baeldung.com/persistence-layer-with-spring-and-hibernate
		
		//https://coderanch.com/t/621607/frameworks/Autowired-find-repository-bean-identified
		
		private Class< T > clazz;
		
		// @Autowired
		 //private SessionFactory sessionFactory;
		 @PersistenceContext(type = PersistenceContextType.TRANSACTION)
		 private EntityManager entityManager;
		
		 public void setClazz( final Class< T > clazzToSet ){
		      clazz = clazzToSet;
		   }
		 
		/*@Bean
		public void setSessionFactory(SessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}*/
		
		
		public GenericDAOImpl () {
			super();
		}
		

		 /**
	     * Save.
	     *
	     * @param entity el entity
	     * @return the t
	     */
		public T save(T entity) {
			entityManager.persist(entity);
	    	return entity;
	    }

	    /**
	     * Update.
	     *
	     * @param entity el entity
	     * @return the t
	     */
		public T update(T entity) {
			entityManager.merge(entity);
			return entity;
	    }

	    /**
	     * Delete.
	     *
	     * @param entity el entity
	     * @return the t
	     */
		public T delete(T entity) {
			entityManager.remove(entity);
	    	return entity;
	    }

	    
		/**
		 * Find.
		 *
		 * @param classs el classs
		 * @param id el id
		 * @return the t
		 */
		public T find(Class<T> classs, K id) {
	    	return entityManager.find(classs, id);
	    }
		
		/**
		 * Creates the named query.
		 *
		 * @param arg0 el arg0
		 * @param parametraMap el parametra map
		 * @return the query
		 */
		public Query createNamedQuery(String arg0, Map<String,Object> parametraMap) {
			Query query = entityManager.createNamedQuery(arg0);
			if (parametraMap != null) {
				for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}
			}
			return query;
		}
		
		
		/**
		 * Creates the query.
		 *
		 * @param arg0 el arg0
		 * @param parametraMap el parametra map
		 * @return the query
		 */
		public Query createQuery(String arg0, Map<String,Object> parametraMap) {
			Query query = entityManager.createQuery(arg0);
			if (parametraMap != null) {
				for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}
			}
			return query;
		}
		
		/**
		 * Creates the native query.
		 *
		 * @param arg0 el arg0
		 * @param parametraMap el parametra map
		 * @return the query
		 */
		public Query createNativeQuery(String arg0, Map<String,Object> parametraMap) {
			Query query = entityManager.createNativeQuery(arg0);
			if (parametraMap != null) {
				for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}
			}
			return query;
		}
		
		/**
		 * Creates the native query.
		 *
		 * @param arg0 el arg0
		 * @param arg1 el arg1
		 * @param parametraMap el parametra map
		 * @return the query
		 */
		public Query createNativeQuery(String arg0, Class<?> arg1, Map<String,Object> parametraMap) {
			Query query = entityManager.createNativeQuery(arg0, arg1);
			if (parametraMap != null) {
				for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}
			}
			return query;
		}
	}

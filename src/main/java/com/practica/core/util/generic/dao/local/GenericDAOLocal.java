package com.practica.core.util.generic.dao.local;

public interface GenericDAOLocal<K,T> {

	
    /**
     * Save.
     *
     * @param entity el entity
     * @return the t
     */
    T save(T entity);

    /**
     * Update.
     *
     * @param entity el entity
     * @return the t
     */
    T update(T entity);

    /**
     * Delete.
     *
     * @param entity el entity
     * @return the t
     */
    T delete(T entity);

    
    /**
     * Find.
     *
     * @param classs el classs
     * @param id el id
     * @return the t
     */
    T find(Class<T> classs, K id);
}

